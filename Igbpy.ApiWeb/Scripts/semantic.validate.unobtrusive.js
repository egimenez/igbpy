﻿/*********************
 * Version de jquery.validate.unobstrusive
 * para usar con la validacion de semantic.
 * 
 * NO REQUIERE de jquery.validate.js, pero necesita que 
 * esté habilitadas en la página las opciones correspondientes a las lineas de Web.Config:
 *   <add key="ClientValidationEnabled" value="true"/>
 *   <add key="UnobtrusiveJavaScriptEnabled" value="true"/>
 */
(function () {
    jQuery(document).ready(function () {

        function initFormValidation(formElem) {

            //Primero elimina cualquier dato de moduleForm que ya pudiera existir
            if (formElem.data('moduleForm'))
                formElem.data('moduleForm').destroy();

            // Obtiene los inputs que se pueden validar y prepara el array donde guardar las validaciones
            var validateInputs = formElem.find('[data-val]'),
                validationFields = [];

            validateInputs.each(function () {
                var inputElem = jQuery(this),
                    rules = [];

                // Se comprueban los atributos para saber que reglas han de establecerse
                if (inputElem.is('[data-val-required]')) {
                    rules.push({
                        type: 'empty',
                        prompt: inputElem.attr('data-val-required'),
                    });
                }
                if (inputElem.is('[data-val-email]')) {
                    rules.push({
                        type: 'email',
                        prompt: inputElem.attr('data-val-email'),
                    });
                }
                if (inputElem.is('[data-val-equalto-other]')) {
                    var otherAttr = inputElem.attr('data-val-equalto-other');
                    otherAttr = otherAttr.slice(otherAttr.lastIndexOf('.') + 1);
                    rules.push({
                        type: 'match[' + otherAttr + ']',
                        prompt: inputElem.attr('data-val-equalto'),
                    });
                }
                if (inputElem.is('[data-val-minlength-min]')) {
                    rules.push({
                        type: 'minLength[' + inputElem.attr('data-val-minlength-min') + ']',
                        prompt: inputElem.attr('data-val-minlength'),
                    });
                }
                if (inputElem.is('[data-val-regex]')) {
                    rules.push({
                        type: 'regExp[/' + inputElem.attr('data-val-regex-pattern') + '/]',
                        prompt: inputElem.attr('data-val-regex'),
                    });
                }
                if (inputElem.is('[data-val-range]')) {
                    rules.push({
                        type: 'integer[' + inputElem.attr('data-val-range-min') + '..' + inputElem.attr('data-val-range-max') + ']',
                        prompt: inputElem.attr('data-val-range'),
                    });
                }

                if (rules.length > 0)
                    validationFields.push({ identifier: inputElem.attr('name'), rules: rules });
            });

            if (validationFields.length > 0) {
                var formObject = {};
                for (var i in validationFields) {
                    formObject[validationFields[i].identifier] = validationFields[i];
                }
                formElem.form({ fields: formObject, inline: true });
            }
        }

        jQuery.fn.form.settings.templates.prompt = function (errors) {
            return jQuery('<div/>')
            .addClass('ui basic red prompt label')
            .html(errors[0])
        };

        // Para cada formulario presente
        jQuery('form').has('[data-val="true"]').each(function () {
            var formElem = jQuery(this);
            initFormValidation(formElem);
        });

        // Se publica la funcion de inicializacion
        window.semanticValidation = {
            initFormValidation: initFormValidation
        };
    });
})();