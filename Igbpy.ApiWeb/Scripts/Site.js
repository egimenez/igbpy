﻿/********
 * Métodos compartidos propios del sitio web
 * 
 ********/
(function () {
    window.Igbpy = {

        defaults: {
            position: { lat: -23.514410626104983, lng: -58.204137681396446 },
            apiSettings: {
                url: '',
                method: 'POST',
                urlData: null,
                data: null,
                contentType: 'application/json',
                beforeSend: null,
                successTest: null,
                onSuccess: null,
                onFailure: null,
                onError: null,
                onComplete: null,
                context: this,
            },
            uploadFileSettings: {
                url: '',
                file: null,
                method: 'PUT',
                fileParam: 'File',
                extraData: null,
                beforeSend: null,
                successTest: null,
                onSuccess: null,
                onError: null,
                onTimeout: null,
                onComplete: null,
            },
            dropzoneSettings: {
                method: 'POST',
                uploadMultiple: false,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                dictDefaultMessage: 'Arrastre aquí los archivos que desea subir',
                dictFallbackMessage: 'Su explorador no soporta la carga de ficheros mediante drag & drop',
                dictFallbackText: 'Por favor, utilice el elemento de subir archivos proporcionado',
                dictInvalidFileType: 'El tipo de fichero no es válido',
                dictFileTooBig: 'El fichero es demasiado grande (pesa {{filesize}} y el máximo es {{maxFilesize}})',
                dictResponseError: 'Error al subir el archivo',
                dictCancelUpload: 'Cancelar',
                dictCancelUploadConfirmation: '¿Está seguro?',
                dictRemoveFile: 'Eliminar',
                dictMaxFilesExceeded: 'Máximo de ficheros alcanzado',
                clickable: true,
            }
        },

        createUrl: function (url, urlData) {
            if (urlData && typeof urlData === 'object') {
                var queryValues = [];
                for (var i in urlData) {
                    var value = urlData[i];
                    if (value)
                        queryValues.push(encodeURIComponent(i) + "=" + encodeURIComponent(urlData[i]));
                }
                url = url.concat((url.indexOf('?') < 0 ? '?' : '&') + queryValues.join('&'));
            }
            return url;
        },

        api: function (settings) {

            settings = jQuery.extend({}, this.defaults.apiSettings, settings);

            var newSettings = null;
            if (settings.beforeSend)
                newSettings = settings.beforeSend(settings);
            else
                newSettings = settings;

            if (!newSettings || !newSettings.url) {
                if (settings.onComplete)
                    settings.onComplete();
                return;
            }
            settings = newSettings;

            var url = this.createUrl(settings.url, settings.urlData);

            jQuery.ajax({
                url: url,
                type: settings.method,
                data: typeof settings.data === 'string' ? settings.data : JSON.stringify(settings.data),
                contentType: settings.contentType,
                processData: false,
                context: settings.context,
                cache: false,
            }).then(function (response) {
                if (!settings.successTest || settings.successTest(response)) {
                    if (settings.onSuccess) {
                        settings.onSuccess(response);
                    }
                }
                else {
                    if (settings.onFailure)
                        settings.onFailure(response);
                    else if (settings.onError) {
                        settings.onError(response);
                    }
                    else {
                        console.log('Error en la operación:', response);
                        notie.alert(3, "Error en la operación ", 2.5);
                    }
                }
            }, function (error) {
                if (settings.onError)
                    settings.onError(error.responseJSON, error);
                else {
                    console.log('Error de comunicacion durante la operacion ajax:', arguments);
                    notie.alert(3, "Error de comunicación", 2.5);
                }
            }).always(function () {
                if (settings.onComplete)
                    settings.onComplete();
            });
        },

        /// Método para subir ficheros
        uploadFile: function (settings) {

            settings = jQuery.extend({}, this.defaults.uploadFileSettings, settings);

            var newSettings = settings;
            if (settings.beforeSend)
                newSettings = settings.beforeSend(settings);

            if (!newSettings || !newSettings.url || !newSettings.file) {
                if (settings.onComplete)
                    settings.onComplete();
                return;
            }

            settings = newSettings;

            var formData = new FormData();
            formData.append(settings.fileParam, settings.file);
            if (settings.extraData) {
                for (var field in settings.extraData) {
                    formData.append(field, settings.extraData[field]);
                }
            }

            var request = new XMLHttpRequest();
            request.open(settings.method, settings.url);
            request.setRequestHeader("Accept", "application/json");
            request.responseType = 'json';
            if (settings.onTimeout || settings.onError) {
                request.addEventListener('timeout', function (e) {
                    if (settings.onTimeout)
                        settings.onTimeout(request.response, settings, e);
                    else
                        settings.onError(request.response, settings, e);
                    if (settings.onComplete)
                        settings.onComplete(request.response, settings);
                });
            }
            if (settings.onError) {
                request.addEventListener('error', function (e) {
                    settings.onError(request.response, settings, e);
                    if (settings.onComplete)
                        settings.onComplete(request.response, settings);
                });
            }
            request.addEventListener('load', function (e) {
                if (request.status === 200) {
                    var response = request.response;
                    if (typeof response === 'string')
                        response = JSON.parse(response);
                    if (!settings.successTest || settings.successTest(response, settings)) {
                        if (settings.onSuccess)
                            settings.onSuccess(response, settings);
                    }
                    else if (settings.onError) {
                        settings.onError(response, settings, e);
                    }
                }
                else if (settings.onError) {
                    settings.onError(request.response, settings, e);
                }
                if (settings.onComplete)
                    settings.onComplete(request.response, settings);
            });
            request.send(formData);
        },

        /* Api de google maps */
        _apiRequestsCallbacks: [],
        getGoogleMapsApi: function (callback) {
            var google = window.google;
            if (!google || !google.maps) {
                Igbpy._apiRequestsCallbacks.push(callback);
            }
            else {
                callback(google.maps);
            }
        },
        initGoogleMapsApi: function () {
            if (google && google.maps) {
                while (Igbpy._apiRequestsCallbacks.length > 0)
                    (Igbpy._apiRequestsCallbacks.shift())(google.maps);
            }
        },

        /* GeoPosicionamiento */
        _currentPosition: null,
        _isSearchingCurrentPosition: false,
        _currentPositionCallbacks: [],
        // Establece la posicion actual e informa a los callbacks de esta
        _setCurrentPosition: function (position, isError) {
            this._currentPosition = position;
            while (this._currentPositionCallbacks.length > 0) {
                var item = this._currentPositionCallbacks.pop();
                if (!isError || !item.dontCallbackOnError)
                    item.callback(position, isError);
            }
        },
        // Obtiene la geoposicion por defecto
        getDefaultPosition: function () {
            return this.defaults.position;
        },
        // Obtiene la geoposicion actual del usuario mediante el navigator del explorador web
        getCurrentPosition: function (callback, dontCallbackOnError) {
            if (this._currentPosition === null) {
                this._currentPositionCallbacks.push({ callback: callback, dontCallbackOnError: dontCallbackOnError });
                if (!this._isSearchingCurrentPosition) {
                    this._isSearchingCurrentPosition = true;
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            this._setCurrentPosition({ lat: position.coords.latitude, lng: position.coords.longitude });
                        }.bind(this),
                        function (error) {
                            console.log('Error al obtener la geoposicion', error);
                            this._setCurrentPosition(this.defaults.position, true);
                        }.bind(this));
                    }
                    else {
                        console.log('No se puede obtener la geoposicion, no hay elemento "navigator.geolocation" en este navegador', navigator);
                        this._setCurrentPosition(this.defaults.position, true);
                    }
                }
            }
            else {
                callback(this._currentPosition);
            }
        },

        /* Dropzone */
        createDropzone: function (selector, settings) {
            if (window.Dropzone) {
                settings = jQuery.extend({}, this.defaults.dropzoneSettings, settings);
                return jQuery(selector).dropzone(settings);
            }
            else {
                console.log('Error: Se requiere el módulo de "Dropzone.js" para hacer funcionar este método');
            }
        },

        /* Mapa */
        // Se crea un mapa de google en el interior del componente señalado por el selector con las configuraciones dadas
        createMap: function (selector, settings) {

        var elem = jQuery(selector);
        if (elem.length > 0) {

            settings = jQuery.extend({}, this.defaults.googleMapsSettings, settings);
            if (!settings.center)
                settings.center = this.getDefaultPosition();

            // Se prepara un objeto con las referencias al mapa, sus marcadores y una API de uso
            var customMap = {
                api: null,
                map: null,
                markers: [],
                resize: function () {
                    if (this.api && this.map) {
                        this.api.event.trigger(this.map, 'resize');
                    }
                },
            };

            // Se inicializa el mapa
            this.getGoogleMapsApi(function () {
                var google = window.google;
                if (google && google.maps) {
                    customMap.api = google.maps;
                    customMap.map = new customMap.api.Map(elem[0], {
                        center: settings.center,
                        zoom: settings.zoom,
                        scrollwheel: settings.scrollwheel,
                    });

                    if (settings.markers.length > 0) {
                        settings.markers.forEach(function (markerSettings, i) {
                            markerSettings = jQuery.extend({ map: customMap.map }, markerSettings);
                            var marker = new customMap.api.Marker(markerSettings);
                            marker.setMap(customMap.map);
                            customMap.markers.push(marker);
                        }, this);
                    }

                    if (settings.onLoad)
                        settings.onLoad(customMap);
                }
                else {
                    console.log('Google Maps API didn\'t load', api);
                }
            }.bind(this));

            // Se almacena en el elemento seleccionado para alojar el mapa
            elem.data('Igbpy.googleMap', customMap);

            // Se retorna también este objeto

            return customMap;
        }
        else {
            console.log('ERROR: (createGoogleMap) No se encuentra ningún elemento con el selector dado');
        }
    },

    createLocationPicker: function (selector, settings) {
        var currentOnLoad = settings.onLoad;
        settings.onLoad = function (customMap) {
            settings = settings || {};
            var locationPickerSettings = jQuery.extend({
                map: customMap.map,
                position: customMap.map.getCenter(),
                draggable: true,
            }, this.defaults.locationPickerSettings, settings.locationPicker);
            customMap.locationPickerMarker = new customMap.api.Marker(locationPickerSettings);

            if (locationPickerSettings.onPositionChange) {
                customMap.locationPickerMarker.addListener('position_changed', function () {
                    locationPickerSettings.onPositionChange(customMap.locationPickerMarker.getPosition());
                });
            }

            if (locationPickerSettings.changePositionOnClick) {
                customMap.map.addListener('click', function (e) {
                    customMap.locationPickerMarker.setPosition(e.latLng);
                });
            }

            if (currentOnLoad)
                currentOnLoad(customMap);
        }.bind(this);
        return this.createMap(selector, settings);
    },
    };
})();