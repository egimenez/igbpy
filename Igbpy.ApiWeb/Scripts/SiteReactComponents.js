﻿"use strict";
if (React && ReactDOM && EventBus && Igbpy) {
    (function () {

        // ----- COMPONENTES ---------

        /// Componente de Paginador
        var PaginatorComponent = React.createClass({
            displayName: 'PaginatorComponent',
            // Mixins
            // Campos propios
            // Definición de propiedades
            propTypes: {
                variantClassNames: React.PropTypes.string,
                offset: React.PropTypes.number,
                size: React.PropTypes.number,
                totalSize: React.PropTypes.number,
                onPageChange: React.PropTypes.func,
                numResultsTextFormat: React.PropTypes.string,
            },
            // Métodos propios
            _handleClick: function (offset, e) {
                e.preventDefault();
                if ((offset < 0 && this.props.offset <= 0) || offset == this.props.offset || offset >= this.props.totalSize)
                    return;
                if (this.props.onPageChange) {
                    this.props.onPageChange(Math.max(offset, 0), this.props.size, e);
                }
            },
            // Métodos de React
            getDefaultProps: function () {
                return {
                    offset: 0,
                    size: 10,
                    totalSize: 0,
                    variantClassNames: '',
                    emptyResultsText: 'No se encontraron resultados',
                    numResultsTextFormat: '{from} a {to} de {total}',
                }
            },
            render: function () {
                var items = [],
                    totalSize = this.props.totalSize,
                    size = this.props.size <= 0 ? 10 : this.props.size,
                    offset = this.props.offset,
                    pages = size > 0 ? Math.ceil(totalSize / size) : 0,
                    activePage = size > 0 ? Math.floor(offset / size) : 0,
                    pageOffset = 0,
                    visibleFrom = activePage - 2,
                    visibleTo = activePage + 2;

                if (totalSize > 0) {
                    items.push(React.createElement('div', { key: 'info', className: 'item' }, this.props.numResultsTextFormat.replace('{from}', (offset + 1).toString()).replace('{to}', (offset + Math.min(size, totalSize - offset)).toString()).replace('{total}', totalSize.toString())));// + ' a ' + (offset + Math.min(size, totalSize - offset)) + ' de ' + totalSize));

                    if (pages > 0) {
                        items.push(React.createElement('a', { key: 'prev', className: 'previous icon item', href: '#', onClick: this._handleClick.bind(this, (offset - size)) },
                                        React.createElement('i', { className: 'left chevron icon' })));

                        for (var i = 0; i < pages; i++) {
                            pageOffset = i * size;
                            if (i == 0 || i == pages - 1 || (i >= visibleFrom && i <= visibleTo)) {
                                if (i == pages - 1 && i > (visibleTo + 1)) {
                                    items.push(React.createElement('div', { key: 'rightCollapsized', className: 'disabled item' }, '...'));
                                }
                                items.push(React.createElement('a', { key: i, className: ((i == activePage ? 'active ' : '') + 'item'), href: '#', onClick: this._handleClick.bind(this, (pageOffset)) }, (i + 1).toString()));
                                if (i == 0 && (visibleFrom - 1) > 0) {
                                    items.push(React.createElement('div', { key: 'leftCollapsized', className: 'disabled item' }, '...'));
                                }
                            }
                        }

                        items.push(React.createElement('a', { key: 'next', className: 'next icon item', href: '#', onClick: this._handleClick.bind(this, (offset + size)) },
                                        React.createElement('i', { className: 'right chevron icon' })));
                    }
                }
                else {
                    items.push(React.createElement('div', { key: 'info', className: 'item' }, this.props.emptyResultsText));
                }

                return React.createElement('div', { className: 'ui pagination ' + this.props.variantClassNames + ' menu' }, items);
            },
        });

        // Componente de Tabla configurable
        var TableComponent = React.createClass({
            displayName: 'TableComponent',
            // Mixins
            // Campos propios
            // Definición de propiedades
            propTypes: {
                variantClassNames: React.PropTypes.string,
                columns: React.PropTypes.arrayOf(React.PropTypes.shape({
                    title: React.PropTypes.string,
                    cellClassName: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]),
                    render: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.func]),
                })),
                items: React.PropTypes.arrayOf(React.PropTypes.object),
                offset: React.PropTypes.number,
                size: React.PropTypes.number,
                totalSize: React.PropTypes.number,
                onPageChange: React.PropTypes.func,
            },
            // Métodos propios
            // Métodos de React
            getDefaultProps: function () {
                return {
                    columns: [],
                    items: [],
                }
            },
            render: function () {
                var headers = [];
                for (var i = 0, iMax = this.props.columns.length; i < iMax; i++) {
                    var column = this.props.columns[i];
                    headers.push(React.createElement('th', { key: i }, column.title || ''));
                }

                var rows = [];
                for (var i = 0, iMax = this.props.items.length; i < iMax; i++) {
                    var item = this.props.items[i],
                        cells = [];

                    for (var j = 0, jMax = this.props.columns.length; j < jMax; j++) {
                        var column = this.props.columns[j],
                            value = null,
                            mustCheck = true;

                        if (column.render) {
                            if (typeof column.render == 'function') {
                                value = column.render(item, i);
                            }
                            else {
                                if (typeof item[column.render] === 'number')
                                    value = item[column.render].toString();
                                else
                                    value = item[column.render] || '';
                            }

                            if (typeof value == 'object' && !Array.isArray(value))
                                value = [value];

                            if (Array.isArray(value)) {
                                mustCheck = false;
                                var values = [];
                                for (var k = 0, kMax = value.length; k < kMax; k++) {
                                    if (typeof value[k] == 'object') {
                                        switch (value[k].type) {
                                            case 'img':
                                                values.push(React.createElement('img', { key: k, src: value[k].src, className: value[k].className, alt: value[k].alt }));
                                                break;
                                            case 'a':
                                                values.push(React.createElement('a', { key: k, href: value[k].href, className: value[k].className, onClick: (value[k].onClick ? value[k].onClick.bind(this, item) : null), title: value[k].title, target: value[k].target }, value[k].text));
                                                break;
                                            case 'button':
                                                values.push(React.createElement('button', { key: k, type: 'button', className: value[k].className, onClick: (value[k].onClick ? value[k].onClick.bind(this, item) : null), title: value[k].title }, value[k].text));
                                                break;
                                            case 'div':
                                                values.push(React.createElement('div', { key: k, className: value[k].className }, value[k].text));
                                                break;
                                            case 'html':
                                                values.push(React.createElement('span', { key: k, className: value[k].className, dangerouslySetInnerHTML: { __html: value[k].text } }));
                                                break;
                                            default:
                                                console.log('El tipo del valor a renderizar no se observa (col #' + j + ', item #' + i + ', elem #' + k + '):', value[k]);
                                                break;
                                        }
                                    }
                                    else if (typeof value[k] == 'string' || typeof value[k] == 'number') {
                                        values.push(React.createElement('span', { key: k }, value[k]));
                                    }
                                    else {
                                        console.log('El tipo del valor a renderizar no es valido (col #' + j + ', item #' + i + ', elem #' + k + '):', value[k]);
                                    }
                                }
                                value = values;
                            }
                        }

                        if (mustCheck && value && typeof value != 'string' && typeof value != 'number') {
                            console.log('El valor renderizado debe ser un string o un numero (col #' + j + ', item #' + i + ')', value);
                            value = null;
                        }

                        var cellClassName = column.cellClassName;
                        if (cellClassName && typeof cellClassName == 'function')
                            cellClassName = cellClassName(item, i);

                        cells.push(React.createElement('td', { key: j, className: cellClassName }, value));
                    }
                    rows.push(React.createElement('tr', { key: i }, cells));
                }

                return React.createElement('table', { className: 'ui ' + this.props.variantClassNames + ' table' },
                            React.createElement('thead', null,
                                React.createElement('tr', null, headers)),
                            React.createElement('tbody', null, rows),
                            React.createElement('tfoot', null,
                                React.createElement('tr', null,
                                    React.createElement('td', { colSpan: Math.max(1, this.props.columns.length), className: 'right aligned' },
                                        React.createElement(PaginatorComponent, this.props, null))))
                    );
            },
        });

        // Elemento de una galería con acciones
        var ActionGalleryItemComponent = React.createClass({
            displayName: 'ActionGalleryItemComponent',
            // Mixins
            // Campos propios
            // Definición de propiedades
            propTypes: {
                src: React.PropTypes.string.isRequired,
                thumbSrc: React.PropTypes.string,
                className: React.PropTypes.string,
                title: React.PropTypes.string,
                actionsContainerClass: React.PropTypes.string,
                actions: React.PropTypes.arrayOf(React.PropTypes.shape({
                    onClick: React.PropTypes.func.isRequired,
                    text: React.PropTypes.string.isRequired,
                    className: React.PropTypes.string,
                    title: React.PropTypes.string,
                    isHtml: React.PropTypes.bool,
                })),
                data: React.PropTypes.any,
                onClick: React.PropTypes.func,
                width: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number])
            },
            // Métodos propios
            _handleClick: function (e) {
                if (this.props.onClick) {
                    if (this.props.onClick(this.props.src, this.props.data, e) == false)
                        e.preventDefault();
                }
            },
            _handleActionClick: function (onClick, e) {
                e.preventDefault();
                onClick(this.props.src, this.props.data, e);
            },
            // Métodos de React
            getDefaultProps: function () {
                return {
                    className: 'ActionGalleryItemComponent',
                    width: '100%',
                    actions: [],
                }
            },
            render: function () {

                var actionsNodes = this.props.actions.map(function (action, i) {
                    var nodeProps = { className: action.className, onClick: this._handleActionClick.bind(this, action.onClick), title: action.title, href: '#' };
                    if (action.isHtml) {
                        nodeProps.dangerouslySetInnerHTML = { __html: action.text };
                    }
                    return React.createElement('a', nodeProps, action.isHtml ? null : action.text);
                }, this);
                if (actionsNodes)
                    actionsNodes = React.createElement('span', { className: this.props.actionsContainerClass }, actionsNodes);

                return React.createElement('div', { className: this.props.className, style: { display: 'inline-block', width: this.props.width } },
                React.createElement('a', { href: this.props.src, target: '_blank', onClick: this._handleClick },
                    React.createElement('div', { style: { width: '100%', position: 'relative' } },
                        React.createElement('span', { style: { paddingTop: '100%', display: 'block' } }),
                        React.createElement('div', { title: this.props.title, style: { backgroundImage: "url('" + (this.props.thumbSrc || this.props.src) + "')", position: 'absolute', width: '100%', height: '100%', top: 0, backgroundSize: 'contain', backgroundPosition: 'center', backgroundRepeat: 'no-repeat' } })
                    )),
                    actionsNodes
                );
            },
        });

        // Galería de elementos con acciones
        var ActionGalleryComponent = React.createClass({
            displayName: 'ActionGalleryComponent',
            // Mixins
            // Campos propios
            // Definición de propiedades
            propTypes: {
                className: React.PropTypes.string,
                itemContainerClassName: React.PropTypes.string,
                itemClassName: React.PropTypes.string,
                items: React.PropTypes.arrayOf(React.PropTypes.shape(ActionGalleryItemComponent.propTypes)),
                defaultActions: ActionGalleryItemComponent.propTypes.actions,
                defaultOnClick: ActionGalleryItemComponent.propTypes.onClick,
            },
            // Métodos propios
            // Métodos de React
            getDefaultProps: function () {
                return {
                    className: 'ActionGalleryComponent',
                    items: [],
                }
            },
            render: function () {
                var defaultProps = {
                    className: this.props.itemClassName,
                    onClick: this.props.defaultOnClick,
                    actions: this.props.defaultActions,
                };
                var itemsNodes = this.props.items.map(function (item, i) {
                    var itemProps = jQuery.extend({ key: item.src }, defaultProps, item);
                    return React.createElement('span', { className: this.props.itemContainerClassName },
                        React.createElement(ActionGalleryItemComponent, itemProps));
                }, this);
                return React.createElement('div', { className: this.props.className }, itemsNodes);
            },
        });


        // ------------ HELPERS ----------------------
        var defaultSettings = {
            createImageSelector: {
                src: null,
                ThumbSrc: null,
                className: 'action-image',
                title: null,
                width: null,
                fileInputSelector: null,
                selectOnClick: false,
                uploadUrl: null,
                uploadMethod: 'POST',
                uploadBeforeSend: null,
                uploadSuccessTest: null,
                onUploadSuccess: null,
                onUploadError: null,
                onUploadComplete: null,
                emptySrc: null,
                emptyThumbSrc: null,
                emptyClassName: null,
                emptyTitle: 'Sin imagen seleccionada',
                canDelete: true,
                confirmDelete: null, // function(function delete())
                deleteUrl: false,
                deleteMethod: 'POST',
                deleteBeforeSend: null,
                deleteSuccessTest: null,
                onDeleteSuccess: null,
                onDeleteError: null,
                onDeleteComplete: null
            }
        }

        function createImageSelector(selector, settings) {

            // Se preparan los settings iniciales
            settings = jQuery.extend({}, defaultSettings.createImageSelector, settings);
            settings.emptyActions = [];
            settings.selectedActions = [];
            settings.onClick = null;
            settings.emptyOnClick = function () { return false; };

            // Se obtiene el elemento donde se contendrá la imagen
            var elem = jQuery(selector);
            if (elem.length > 0) {
                // Se prepara el elemento de propiedades con sus datos básicos
                var props = {
                    src: settings.src || settings.emptySrc,
                    thumbSrc: settings.thumbSrc || settings.emptyThumbSrc,
                    className: settings.src ? settings.className : (settings.emptyClassName || settings.className),
                    title: settings.title ? settings.title : (settings.emptyTitle || settings.title),
                    actionsContainerClass: 'actions',
                    actions: settings.src?settings.selectedActions:settings.emptyActions,
                    data: null,
                    onClick: null,
                    width: settings.width
                };

                // Se prepara la funcion para renderizar el elemento
                var renderImageSelector = function (props) {
                    if (props.src != settings.emptySrc || settings.selectOnClick)
                        props.onClick = settings.onClick;
                    else
                        props.onClick = settings.emptyOnClick;
                    ReactDOM.render(React.createElement(ActionGalleryItemComponent, props), elem[0]);
                };

                // Se comprueba que exista el fileInput, y si no, se crea uno y se inserta tras el componente
                var fileInputElem = jQuery(settings.fileInputSelector);
                if (fileInputElem.length === 0) {
                    fileInputElem = jQuery('<input type="file" style="display:none">');
                    elem.after(fileInputElem);
                }

                // Se preparan las acciones que serán necesarias
                if (settings.selectOnClick) {
                    settings.onClick = function () {
                        fileInputElem.click();
                        return false;
                    };
                }
                else {
                    var selectAction = {
                        onClick: function () {
                            fileInputElem.click();
                            return false;
                        },
                        text: '<i class="folder icon"></i>',
                        className: 'ui small circular icon action button',
                        title: 'Seleccionar',
                        isHtml: true,
                    };
                    settings.emptyActions.push(selectAction);
                    settings.selectedActions.push(selectAction);
                }

                if (settings.canDelete) {
                    var deleteImage = function () {
                        if (!settings.deleteUrl) {
                            props.src = settings.emptySrc;
                            props.thumbSrc = settings.emptyThumbSrc,
                            props.title = settings.emptyTitle || settings.title;
                            props.className = settings.emptyClassName || settings.className;
                            props.actions = settings.emptyActions,
                            renderImageSelector(props);
                        }
                        else {
                            Igbpy.api({
                                url: settings.deleteUrl,
                                method: settings.deleteMethod,
                                beforeSend: function (deleteSettings) {
                                    if (settings.deleteBeforeSend)
                                        return settings.deleteBeforeSend(deleteSettings);
                                    return deleteSettings;
                                },
                                successTest: function (response) {
                                    if (settings.deleteSuccessTest)
                                        return settings.deleteSuccessTest(response);
                                    return true;
                                },
                                onSuccess: function (response) {
                                    if (settings.onDeleteSuccess)
                                        settings.onDeleteSuccess(response);
                                    props.src = settings.emptySrc;
                                    props.thumbSrc = settings.emptyThumbSrc,
                                    props.title = settings.emptyTitle || settings.title;
                                    props.className = settings.emptyClassName || settings.className;
                                    props.actions = settings.emptyActions,
                                    renderImageSelector(props);
                                },
                                onError: function (response, e) {
                                    if (settings.onDeleteError)
                                        settings.onDeleteError(response);
                                },
                                onTimeout: function (response, e) {
                                    if (settings.onDeleteError)
                                        settings.onDeleteError(response);
                                },
                                onComplete: function () {
                                    if (settings.onDeleteComplete)
                                        settings.onDeleteComplete();
                                }
                            });
                        }
                    }

                    var deleteAction = {
                        onClick: function () {
                            if (settings.confirmDelete)
                                settings.confirmDelete(deleteImage);
                            else
                                deleteImage();
                        },
                        text: '<i class="remove icon"></i>',
                        className: 'ui small circular icon secondary action button',
                        title: 'Eliminar',
                        isHtml: true,
                    };

                    settings.selectedActions.push(deleteAction);
                }

                // Se controla el cambio de valor en el input
                fileInputElem.change(function (e) {
                    var input = e.target;
                    if (input.files.length > 0) {
                        // Si no tiene información de subida de fichero, simplemente se muestra el actual
                        if (!settings.uploadUrl) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                props.src = e.target.result;
                                props.thumbSrc = null;
                                props.className = settings.className;
                                props.title = settings.title;
                                props.actions = settings.selectedActions,
                                renderImageSelector(props);
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                        else {
                            Igbpy.uploadFile({
                                url: settings.uploadUrl,
                                method: settings.uploadMethod,
                                file: input.files[0],
                                fileParam: 'Imagen',
                                beforeSend: function (uploadSettings) {
                                    if (settings.uploadBeforeSend)
                                        return settings.uploadBeforeSend(uploadSettings);
                                    return uploadSettings;
                                },
                                successTest: function (response) {
                                    if (settings.uploadSuccessTest)
                                        return settings.uploadSuccessTest(response);
                                    return true;
                                },
                                onSuccess: function (response) {
                                    var item = null;
                                    if (settings.onUploadSuccess)
                                        item = settings.onUploadSuccess(response);
                                    if (!item)
                                        item = response;
                                    props.src = item.src;
                                    props.thumbSrc = item.thumbSrc;
                                    props.className = item.className || props.className;
                                    props.title = item.title || props.title;
                                    props.actions = settings.selectedActions,
                                    renderImageSelector(props);
                                },
                                onError: function (response, e) {
                                    if (settings.onUploadError)
                                        settings.onUploadError(response);
                                },
                                onTimeout: function (response, e) {
                                    if (settings.onUploadError)
                                        settings.onUploadError(response);
                                },
                                onComplete: function () {
                                    if (settings.onUploadComplete)
                                        settings.onUploadComplete();
                                }
                            });
                        }
                    }
                });

                // Se renderiza el componente por primera vez con sus propiedades
                renderImageSelector(props);
            }
            else {
                console.log('ERROR: el selector proporcionado no corresponde a ningun elemento en la página');
            }
        }

        // ------------ OBJETO PUBLICO ---------------
        window.IgbpyComponents = {
            // Paginador simple
            // Propiedades: 
            // - variantClassNames: string, 
            // - totalSize: number, 
            // - size: number, 
            // - offset: number, 
            // - onPageChange: function(offset, size)
            paginator: PaginatorComponent,

            // Tabla configurable
            // Propiedades:
            // - updateEventName(!): string, 
            // - variantClassNames: string, 
            // - columns: array,
            // - defaultItems: array,
            // - defaultTotalSize: number, 
            // - defaultSize: number, 
            // - defaultOffset: number, 
            // - onPageChange: function(offset, size)
            table: TableComponent,

            // Imagen sobre la que se pueden aplicar acciones
            // - src: string(!),
            // - thumbSrc: string,
            // - className: string,
            // - title: string
            // - width: string || number,
            // - data: any,
            // - actions: array[{ 
            // > - onClick: function(src, e, data)(!), 
            // > - text: string(!), 
            // > - isHtml: bool, 
            // > - title: string, 
            // > - className: string 
            // > }],
            // - onClick: function(src, e, data)
            actionImage: ActionGalleryItemComponent,

            // Galería con elementos a los que se pueden aplicar acciones
            // - className: string, 
            // - itemClassName: string,
            // - itemContainerClassName: string,
            // - items: array[{
            // > - src: string(!),
            // > - thumbSrc: string,
            // > - className: string,
            // > - title: string
            // > - width: string || number,
            // > - data: any,
            // > - actions: [{ 
            // > > - onClick: function(src, e, data)(!), 
            // > > - text: string(!), 
            // > > - isHtml: bool, 
            // > > - title: string, 
            // > > - className: string 
            // > > }],
            // > - onClick: function(src, e, data)
            // }],
            // - defaultActions: (igual que items[].action),
            // - defaultOnClick: function(src, e, data),
            gallery: ActionGalleryComponent,

            // HELPERS
            helpers: {
                // Configuraciones por defecto para los distintos helpers
                defaultSettings: defaultSettings,
                // Crea un selector de imágenes configurable con el que se pueden automatiar la tareas
                // de subida/cambio y eliminación de una imagen
                createImageSelector: createImageSelector
            }
        };

    })(React, ReactDOM, EventBus, Igbpy);
}
else {
    console.log('Se requieren Igbpy, React, ReactDOM y EventBus para usar el módulo de IgbpyComponents');
}