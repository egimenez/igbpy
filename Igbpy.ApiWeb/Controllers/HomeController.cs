﻿using Igbpy.ApiWeb.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Igbpy.ApiWeb.Controllers
{
    public class HomeController : IgbpyController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Asistencia()
        {
            return View();
        }

        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult Nosotros()
        {
            return View();
        }

        public ActionResult Noticias()
        {
            return View();
        }

        [Authorize]
        public ActionResult Private()
        {
            return View();
        }
    }
}
