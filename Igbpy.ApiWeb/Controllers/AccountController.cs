﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Igbpy.ApiWeb.Models;
using Igbpy.ApiWeb.Results;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.IdentityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Igbpy.Domain;
using Igbpy.ApiWeb.Models.Account;

namespace Igbpy.ApiWeb.Controllers
{
    [Authorize]
    public class AccountController : IgbpyController
    {

        private IgbpyContext.IgbpyUserManager _userManager;

        public IgbpyContext.IgbpyUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<IgbpyContext.IgbpyUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private SignInManager<User, string> _signInManager;
        public SignInManager<User, string> SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<SignInManager<User, string>>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        #region Util
        public async Task<User> FindAsync(UserLoginInfo loginInfo)
        {
            User user = await _userManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(User user)
        {
            var result = await UserManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        [NonAction]
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        [NonAction]
        private void PrepareLoginModel(LoginViewModel model, string returnTo = null)
        {
            IEnumerable<AuthenticationDescription> descriptions = Request.GetOwinContext().Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();
            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.HttpRouteUrl("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        redirect_uri = new Uri(Request.Url, "/").AbsoluteUri,
                        return_url = returnTo!=null? new Uri(Request.Url, returnTo).AbsoluteUri: null,
                        state = ""
                    }),
                };
                logins.Add(login);
            }

            model.ExternalLogins = logins;
        }

        #endregion

        // GET: Login
        [AllowAnonymous]
        public ActionResult Login(string returnTo = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnTo))
                    return Redirect(returnTo);
                //return RedirectToAction("Private", "Home");
                if (User.IsInRole("admin"))
                    return RedirectToAction("Index", "Home", new { area = "Admin" });
                return RedirectToAction("Index", "Home");
            }

            LoginViewModel model = new LoginViewModel();

            PrepareLoginModel(model, returnTo);

            ViewData["returnTo"] = returnTo;
            return View(model);
        }

        // POST: Login
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnTo = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnTo))
                    return Redirect(returnTo);
                //return RedirectToAction("Private", "Home");
                if (User.IsInRole("admin"))
                    return RedirectToAction("Index", "Home", new { area = "Admin" });
                return RedirectToAction("Index", "Home");
            }

            if (ModelState.IsValid)
            {
                var  manager = Request.GetOwinContext().Get<SignInManager<User, string>>();
                var result = manager.PasswordSignIn(model.Email, model.Password, true, false);
                if (result == SignInStatus.Success)
                {
                    if (!string.IsNullOrEmpty(returnTo))
                        return Redirect(returnTo);
                    //return RedirectToAction("Private", "Home");
                    if (User.IsInRole("admin"))
                        return RedirectToAction("Index", "Home", new { area = "Admin" });
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "No existe el usuario y/o contraseña");
            }

            PrepareLoginModel(model, returnTo);

            ViewData["returnTo"] = returnTo;
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register(string returnTo)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnTo))
                    return Redirect(returnTo);
                return RedirectToAction("Index", "Home");
            }

            ViewData["returnTo"] = returnTo;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, string returnTo)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnTo))
                    return Redirect(returnTo);
                return RedirectToAction("Index", "Home");
            }

            if (ModelState.IsValid)
            {
                User usuario = new User(model.Email)
                {
                    Email = model.Email,
                    Platform = Domain.Shared.Globals.Platforms.Web,
                    Name = model.Name,
                    PhoneNumber = model.PhoneNumber
                };
                var result = await UserManager.CreateAsync(usuario, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(usuario, true, true);
                    if (!string.IsNullOrEmpty(returnTo))
                        return Redirect(returnTo);
                    return RedirectToAction("Index", "Home");
                }
                foreach (var error in result.Errors)
                    ModelState.AddModelError("", error);
            }

            ViewData["returnTo"] = returnTo;
            return View(model);
        }


        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                //if (user == null || !(await Db.UserManager.IsEmailConfirmedAsync(user.Id)))
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771

                // Se envia un mail con la informacion para el reseteo del password
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, Resources.Resources.Email_ResetPassword_Subject, string.Format(Resources.Resources.Email_ResetPassword_Body, callbackUrl));
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            ViewData["Errors"] = result.Errors;

            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public ActionResult Logout()
        {
            var manager = Request.GetOwinContext().Get<SignInManager<User, string>>();
            manager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }
    }
}