﻿using Igbpy.ApiWeb.Models.Manage;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain;
using Igbpy.Domain.IdentityModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Igbpy.ApiWeb.Controllers
{
    [Authorize]
    public class ManageController : IgbpyController
    {

        private IgbpyContext.IgbpyUserManager _userManager = null;
        protected IgbpyContext.IgbpyUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = HttpContext.GetOwinContext().Get<IgbpyContext.IgbpyUserManager>();
                return _userManager;
            }
        }

        // GET: Manage
        public async Task<ActionResult> Index()
        {
            var model = new IndexViewModel
            {
                HasPassword = await UserManager.HasPasswordAsync(User.Identity.GetUserId()),
            };
            return View(model);
        }

        // GET: Manage/ChangePassword
        public async Task<ActionResult> ChangePassword()
        {
            var model = new ChangePasswordModel
            {
                HasPassword = await UserManager.HasPasswordAsync(User.Identity.GetUserId()),
            };
            return View(model);
        }

        // POST: Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            // Se comprueba el modelo primero
            User usuario = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            bool hasPassword = !string.IsNullOrEmpty(usuario.PasswordHash);

            if (!hasPassword)
            {
                if (string.IsNullOrEmpty(model.CurrentPassword))
                    ModelState.AddModelError("model.CurrentPassword", Resources.Resources.Common_ValidationError_Required);
                else if (!await UserManager.CheckPasswordAsync(usuario, model.CurrentPassword))
                    ModelState.AddModelError("model.CurrentPassword", string.Format(Resources.Resources.Common_ValidationError_Compare, Resources.Resources.Manage_ChangePassword_Fields_ConfirmNewPassword, Resources.Resources.Manage_ChangePassword_Fields_NewPassword));
            }

            if (ModelState.IsValid)
            {
                if (!hasPassword || await UserManager.CheckPasswordAsync(usuario, model.CurrentPassword))
                {
                    // Se agrega o modifica el password
                    IdentityResult result = null;
                    if (!hasPassword)
                        result = await UserManager.AddPasswordAsync(usuario.Id, model.NewPassword);
                    else
                        result = await UserManager.ChangePasswordAsync(usuario.Id, model.CurrentPassword, model.NewPassword);


                    if (result.Succeeded)
                        ViewData["Success"] = true;
                    else
                    {
                        Logger.Error("ManageController - Error al modificar el password\n" + string.Join("\n", result.Errors.Select(s => " - " + s)));
                        ViewData["Error"] = Resources.Resources.Manage_ChangePassword_ErrorMessages_Update;
                    }
                }
                else
                {
                    ViewData["Error"] = Resources.Resources.Manage_ChangePassword_ErrorMessages_WrongCurrentPassword;
                }
            }
            else
            {
                ViewData["Error"] = Resources.Resources.Common_ErrorMessages_ValidationError;
            }

            model.HasPassword = hasPassword;
            return View(model);
        }
    }
}