﻿using Igbpy.ApiWeb.Shared;
using Igbpy.Domain;
using Igbpy.Domain.Extensions.Messaging;
using Igbpy.Domain.Extensions.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Igbpy.ApiWeb
{
    /// <summary>
    /// Clase que contiene los datos y métodos para configurar las tareas programadas de la aplicación
    /// </summary>
    public static class ScheduledTasksConfig
    {
        private static System.Threading.Timer _qeuedEmailSenderTimer { get; set; }

        /// <summary>
        /// Inicialización
        /// </summary>
        public static void Initialize()
        {
            // Envío de Emails (comienza inmediatamente y envía cada 5 minutos => 300.000 milisegundos)
            _qeuedEmailSenderTimer = new System.Threading.Timer(new QueuedEmailSender().Execute, null, 0, 300000);
        }

        /// <summary>
        /// Clase que se ocupa del envío y actualización de las entidades de emails en cola
        /// </summary>
        private class QueuedEmailSender
        {
            public void Execute(object state)
            {
                log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                try
                {
                    using (IgbpyContext db = new IgbpyContext())
                    {
                        // Se obtienen los settings de envio de correo

                        // Se obtienen todos los mensajes en cola que no se hayan enviado y tengan menos de 3 reintentos
                        // Para cada uno se crea un nuevo mensaje de email a enviar, y se relacionan mediante un diccionario para poder controlar
                        // su estado cuando se envíen (o si fallan).
                        var mailMessagesDictionary = db.QueuedEmails.Where(qe => !qe.IsSent && qe.PendingRetriesCount > 0).OrderBy(qe => qe.CreateDate).ToArray().Select(x =>
                        {
                            // Lo primero es que se actualiza la fecha de envio de la entidad (sin guardar la informacion todavía)
                            x.LastTryDate = DateTime.Now;

                            // Se prepara el email a enviar
                            MailMessage message = new MailMessage(
                            new MailAddress(x.FromAddress, x.FromName),
                            new MailAddress(x.ToAddress, x.ToName))
                            {
                                Subject = x.Subject,
                                Body = x.Body,
                                IsBodyHtml = x.IsBodyHtml,
                            };

                            if (!string.IsNullOrEmpty(x.CC))
                                message.CC.Add(x.CC);

                            if (!string.IsNullOrEmpty(x.Bcc))
                                message.Bcc.Add(x.Bcc);

                            if (!string.IsNullOrEmpty(x.ReplyTo))
                                message.ReplyToList.Add(x.ReplyTo);

                            if (!string.IsNullOrEmpty(x.AttachmentFilePath))
                                message.Attachments.Add(new Attachment(x.AttachmentFilePath));

                            return new { Message = message, Entity = x };
                        }).ToDictionary(x => x.Message, x => x.Entity);

                        // Se envían todos los mensajes
                        db.SendEmails(mailMessagesDictionary.Select(x => x.Key).ToArray(), m =>
                        {
                            // Si no hay errores, se modifica la entidad como enviada
                            mailMessagesDictionary[m].IsSent = true;
                        }, (m, e) =>
                        {
                            var entity = mailMessagesDictionary[m];
                            entity.PendingRetriesCount--;
                            entity.LastErrorMessage = e.ToString();
                            logger.Error($"QueuedEmailSender - Error al enviar el email en cola #{entity.Id}", e);
                        });

                        // Finalmente se guardan los cambios en la base de datos
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    logger.Error("QueuedEmailsSender", e);
                }
            }
        }
    }
}