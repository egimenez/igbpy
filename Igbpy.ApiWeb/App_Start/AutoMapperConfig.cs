﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.App_Start
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration mapperConfig;

        public static MapperConfiguration dynamicMapper;

        public static void Configure()
        {
            dynamicMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
            });

            mapperConfig = new MapperConfiguration(cfg =>
            {
                // 

                ////clientes
                //cfg.CreateMap<ClientModel, Domain.Models.Client>()
                //.ForMember(s => s.User, opt => opt.Ignore())
                //.ForMember(s => s.Id, opt => opt.Ignore());
                //cfg.CreateMap<ClientModel, User>()
                //.ForMember(s => s.Id, opt => opt.Ignore());
                //cfg.CreateMap<Domain.Models.Client, ClientModel>();
                //cfg.CreateMap<Domain.Models.Client, Areas.Admin.Models.Clients.CreateEditViewModel>();
                //cfg.CreateMap<Domain.Models.Client, Areas.Manage.Models.Clients.CreateEditViewModel>();

                ////proveedores
                //cfg.CreateMap<Areas.Admin.Models.Suppliers.CreateEditViewModel, Domain.Models.Supplier>()
                //   .ForMember(s => s.Id, opt => opt.Ignore());
                //cfg.CreateMap<Domain.Models.Supplier, Areas.Admin.Models.Suppliers.CreateEditViewModel>();

                ////clientes autorizaciones
                //cfg.CreateMap<ClientAuthorization, Billio.ApiWeb.Models.Api.ClientAuthorizationModel>();
                //cfg.CreateMap<Billio.ApiWeb.Models.Api.ClientAuthorizationModel, ClientAuthorization>().ForMember(s => s.Id, opt => opt.Ignore());

          //      cfg.CreateMap<Document, Billio.ApiWeb.Models.Api.DocumentModel>()
          //.ForMember(s => s.DocumentDate, opt => opt.MapFrom(x => (x.DocumentDate.HasValue ? x.DocumentDate.Value.ToShortDateString() : "")));
            });
        }
    }
}