﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Igbpy.ApiWeb.Models;
using Igbpy.Domain;
using Microsoft.AspNet.Identity.Owin;
using Igbpy.Domain.IdentityModels;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Owin.Security.Facebook;
using System.Configuration;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Extensions.Settings;
using Igbpy.Domain.Extensions.Messaging;

namespace Igbpy.ApiWeb
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static CookieAuthenticationOptions CookieOptions { get; set; }

        // Para más informacion para configurar la autenticacion, por favor, visitar http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            #region Instancias accesibles desde el OwinContext en cada Request

            // Configura el contexto de base de datos y el user manager para que haya una sola instancia por solicitud
            app.CreatePerOwinContext<IgbpyContext>(() => { return new IgbpyContext(); });

            // Se crea el elemento de validación y busqueda de usuarios
            app.CreatePerOwinContext<IgbpyContext.IgbpyUserManager>((IdentityFactoryOptions<IgbpyContext.IgbpyUserManager> options, IOwinContext context) =>
            {
                var db = context.Get<IgbpyContext>();
                var userManager = new IgbpyContext.IgbpyUserManager(new UserStore<User>(db));
                var dataProtectionProvider = options.DataProtectionProvider;
                if (dataProtectionProvider != null)
                {
                    userManager.UserTokenProvider = new DataProtectorTokenProvider<User>(dataProtectionProvider.Create("ASP.NET Identity"));
                }

                userManager.EmailService = new EmailService();

                return userManager;
            });

            // Se crea el elemento de adminsitracion de los logins (sólo web)
            app.CreatePerOwinContext<SignInManager<User, string>>((IdentityFactoryOptions<SignInManager<User, string>> options, IOwinContext context) =>
            {
                return new SignInManager<User, string>(context.Get<IgbpyContext.IgbpyUserManager>(), context.Authentication);
            });

            #endregion

            #region Configuración de la autenticacion por Application Cookie

            // Configura la aplicacion para que permita el login por la web con su cookie de aplicacion
            CookieOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                ReturnUrlParameter = "returnTo",
                Provider = new CookieAuthenticationProvider
                {
                    // Habilita a la aplicacion a validar el sello de seguridad cuando el usuario ingresa.
                    // Esta es una capacidad de seguridad que se utiliza cuando se cambia el password o se agrega un login externo a la cuenta.
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<UserManager<User>, User>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie)),
                },
            };

            app.UseCookieAuthentication(CookieOptions);

            #endregion

            #region Configuracion de la autorización mediante Token

            // Configura y habilita la aplicacion para usar el flujo de autenticacion de OAuth local mediante token
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                // - La llamada para obtener el token debe ser un post a esta ruta con el cuerpo con los siguientes datos
                // username=****&password=****&grant_type=password
                // - Posteriormente las llamadas deberán tener en un header:
                // Authorization: Bearer ****** <= token
                TokenEndpointPath = new PathString("/token"),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // TODO Establecer en produccion: AllowInsecureHttp = false
                AllowInsecureHttp = true,
                //Provider = new ApplicationOAuthProvider(),
                Provider = new OAuthAuthorizationServerProvider
                {
                    // INFO: Actualmente se aceptan todos los clientes
                    OnValidateClientAuthentication = context => {
                        return Task.FromResult(context.Validated());
                        },
                    // INFO: Cuando se invoca con el 'grant_type=password', se lleva a cabo una autenticacion
                    OnGrantResourceOwnerCredentials = async context =>
                    {
                        var userManager = context.OwinContext.GetUserManager<IgbpyContext.IgbpyUserManager>();

                        User user = await userManager.FindAsync(context.UserName, context.Password);

                        ClaimsIdentity oAuthIdentity = null;

                        if (user == null)
                        {
                            context.SetError("not_authorized", Resources.Resources.Common_ErrorMessages_NotAuthorized);

                            //oAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
                            //oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, "Guest"));
                            //oAuthIdentity.AddClaim(new Claim(ClaimTypes.Role, "Guest"));
                            //oAuthIdentity.AddClaim(new Claim("Valor raro", "Cosa rara que nos viene bien tener"));
                        }
                        else
                        {
                            oAuthIdentity = await userManager.CreateIdentityAsync(user, OAuthDefaults.AuthenticationType);
                        }

                        if (oAuthIdentity != null)
                            context.Validated(oAuthIdentity);
                    },
                    // INFO: Actualmente permite cualquier redirección desde cualquier cliente
                    OnValidateClientRedirectUri = context =>
                    {
                        return Task.FromResult(context.Validated());
                    }
                }
            };

            app.UseOAuthBearerTokens(OAuthOptions);

            #endregion

            #region Configuración de la autorización externa (Google, Facebook, etc)

            // Habilita la aplicacion para que permita el uso de cookies externas para los logins de redes sociales
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Descomentar las siguientes secciones para habilitar el uso de login con redes sociales
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            // Facebook options
            //FacebookAuthenticationOptions facebookOptions = new FacebookAuthenticationOptions
            //{
            //    // Modificar con los datos de la verdadera APP de Facebook
            //    AppId = "",
            //    AppSecret = "",
            //    SendAppSecretProof = true,
            //    Provider = new FacebookAuthenticationProvider
            //    {
            //        OnAuthenticated = async ctx =>
            //        {
            //            await Task.Run(() =>
            //            {
            //                // (optativo) Se pueden agregan una serie de claims con información en el formato que nos interesa
            //                //var userCtx = ctx.User;

            //                ////ctx.Identity.AddClaim(new Claim(ClaimTypes.Email, (string)userCtx["email"], ClaimTypes.Email, "Facebook"));
            //                //ctx.Identity.AddClaim(new Claim(ClaimTypes.GivenName, (string)userCtx["first_name"], ClaimValueTypes.String, "Facebook"));
            //                //ctx.Identity.AddClaim(new Claim(ClaimTypes.Surname, (string)userCtx["last_name"], ClaimValueTypes.String, "Facebook"));

            //                //// Si se ha llegado hasta aquí, entonces sabemos que el usuario debe ser mayor de edad, porque la aplicación de facebook lo requerirá
            //                //ctx.Identity.AddClaim(new Claim("ageOver18", true.ToString(), ClaimValueTypes.Boolean, null, "Facebook"));

            //            });
            //        },
            //    },
            //};

            //facebookOptions.Scope.Add("public_profile");
            //facebookOptions.Scope.Add("email");

            //app.UseFacebookAuthentication(facebookOptions);

            // Configura y habilita la autenticacion con Google (si está configurada)
            var settings = ConfigurationManager.AppSettings;
            if (!string.IsNullOrWhiteSpace(settings[Globals.AppSettings.GoogleClientId]) &&
                !string.IsNullOrWhiteSpace(settings[Globals.AppSettings.GoogleClientSecret]))
            {
                GoogleOAuth2AuthenticationOptions googleOptions = new GoogleOAuth2AuthenticationOptions()
                {
                    // Modificar con los datos de la verdadera APP de Google
                    ClientId = settings[Globals.AppSettings.GoogleClientId],
                    ClientSecret = settings[Globals.AppSettings.GoogleClientSecret],

                    Provider = new GoogleOAuth2AuthenticationProvider
                    {
                        //OnApplyRedirect = ctx =>
                        //{
                            
                        //},

                        // INFO: Aquí podemos comprobar que es lo que nos ha devuelto Google
                        // y modificar sus valores si es menester.
                        // Tras pasar por aquí, nuestro usuario no está realmente autenticado EN NUESTRA aplicacion, pero hay informacion
                        // suya en los requests que acepten "ExternalCookie", por lo que estará AUTORIZADO.
                        OnAuthenticated = ctx =>
                        {
                            // Se incluye en el request el token de acceso externo que se utilizará más adelante
                            // ¿Es realmente necesario?
                            //ctx.Identity.AddClaim(new Claim("ExternalAccessToken", ctx.AccessToken));
                            return Task.FromResult<object>(null);
                        },

                        OnReturnEndpoint = ctx =>
                        {
                            return Task.FromResult<object>(null);
                        }
                    },
                };
                googleOptions.Scope.Add("email");
                googleOptions.Scope.Add("https://www.googleapis.com/auth/plus.login");

                app.UseGoogleAuthentication(googleOptions);
            } 
            #endregion
        }

        private class EmailService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {
                return Task.Factory.StartNew(() =>
                {
                    using (IgbpyContext db = new IgbpyContext())
                    {
                        var mailMessage = new System.Net.Mail.MailMessage
                        {
                            Subject = message.Subject,
                            Body = message.Body,
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(message.Destination);

                        db.QueueEmails(new[] { mailMessage });
                        db.SaveChanges();
                    }
                });
            }
        }
    }
}
