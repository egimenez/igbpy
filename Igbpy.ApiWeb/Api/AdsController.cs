﻿using Igbpy.ApiWeb.Models;
using Igbpy.ApiWeb.Models.Api;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Igbpy.ApiWeb.Api
{
    [IgbpyApiAuthorize(Roles = Domain.Shared.Globals.Roles.Admin)]
    [RoutePrefix("api/Ads")]
    public class AdsController : IgbpyApiController
    {


        #region Utils

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetNewAdFileName(string filename)
        {
            return "ad_" + DateTime.Now.ToString("yyyyMMddHHssffff") + filename;
        }

        /// <summary>
        /// Mapea los elementos de una query a elementos de modelo manejables
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<AdModel> MapModel(IQueryable<Ads> query, HttpContext httpContext)
        {
            // Se prepara la query para obtener todos los datos en bruto en el objeto de modelo
            // Los datos que necesiten procesado posterior (fechas que convertir en string, urls que 
            // convertir en absolutas, etc) se almacenan separadamente
            var tempQuery = query.Select(x => new
            {
                Model = new AdModel
                {
                    // Se cargan todos los campos posibles, salvo aquellos que requieran procesado
                    Id = x.Id,
                    //ImageUrl = !string.IsNullOrEmpty(x.ImageUrl) ? Url.Content(x.ImageUrl) : null,
                    //ImageThumbUrl
                    Order = x.Order,
                    Seconds = x.Seconds,
                    Deleted = x.Deleted,
                },
                // Ahora los valores que se necesiten procesar
                ImageUrl = x.ImageUrl
            });

            // Se llama a ToArray() para que se ejecute la query y se tengan los datos cargados, se
            // hace un último select en el cual se cargan en el objeto de modelo los datos que requirieran procesado
            // y se retorna
            return tempQuery.ToArray().Select(x =>
            {
                // Si hay una url cargada, se procesa correctamente y se guarda en el objeto de modelo
                if (!string.IsNullOrEmpty(x.ImageUrl))
                {
                    x.Model.ImageUrl = Url.Content(x.ImageUrl);
                    string imageThumbUrl = Helpers.GetThumbnailUrl(x.ImageUrl, httpContext, true);
                    if (imageThumbUrl != null)
                        x.Model.ImageThumbUrl = Url.Content(imageThumbUrl);
                }
                // Para cada resultado, se devuelve un objeto de modelo con los datos procesados
                return x.Model;
            });
        }

        #endregion

        // GET: /api/Ads/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OverrideAuthorization]
        [IgbpyApiAuthorize]
        public async Task<IHttpActionResult> Get([FromUri]QueryRequestModel model)
        {
            QueryResponseModel<AdModel> result = null;
            HttpContext httpContext = HttpContext.Current;

            // Se obtienen los resultados
            try
            {
                result = await GetQueryResponseAsync(model.Offset, model.Size, () => Db.Ads.AsNoTracking().Where(x => model.IncludeDeleted || !x.Deleted).OrderBy(x => x.Order), query => MapModel(query, httpContext));

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("API - AdsController", e);
                result.Error = Resources.Resources.Common_ErrorMessages_QueryItemsFailure;
            }

            return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
        }

        // GET: /api/Ads/{id}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OverrideAuthorization]
        [IgbpyApiAuthorize]
        public async Task<IHttpActionResult> Get(int id)
        {
            QueryResponseModel<AdModel> result = null;
            HttpContext httpContext = HttpContext.Current;
            // Se obtienen el resultado
            try
            {
                result = await GetQueryResponseAsync(0, 1, () => Db.Ads.AsNoTracking().Where(x => x.Id == id), query => MapModel(query, httpContext));
                if (result.TotalSize != 1)
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("API - AdsController", e);
                result.Error = Resources.Resources.Common_ErrorMessages_QueryItemsFailure;
            }

            return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
        }

        // POST: /api/Ads/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Post()
        {
            var result = new CreateResponseModel<int>();
            var model = new AdModel();
            var request = HttpContext.Current.Request;
            var file = new FileUploadResponseModel();
            int order = 0, seconds = 0;

            // Debe enviarse como multipart, porque debe incluir el fichero
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Error(HttpStatusCode.UnsupportedMediaType, Resources.Resources.Common_ErrorMessages_InvalidOperation);
            }
            else
            {
                // Se obtienen los valores del nuevo elemento y se validan los campos
                if (request.Files.Count <= 0)
                    ModelState.AddModelError("Image", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Ads_Fields_ImageUrl));

                if (!request.Params.AllKeys.Contains("Order"))
                    ModelState.AddModelError("Order", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Ads_Fields_Order));
                else if (!Int32.TryParse(request.Params["Order"], out order) || order <= 0)
                    ModelState.AddModelError("Order", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Ads_Fields_Order, 1, Int32.MaxValue));
                else
                    model.Order = order;

                if (!request.Params.AllKeys.Contains("Seconds"))
                    ModelState.AddModelError("Order", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Ads_Fields_Seconds));
                else if (!Int32.TryParse(request.Params["Seconds"], out seconds) || seconds <= 0)
                    ModelState.AddModelError("Order", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Ads_Fields_Seconds, 1, Int32.MaxValue));
                else
                    model.Seconds = seconds;

                // Si es válido, se continúa
                if (ModelState.IsValid)
                {
                    try
                    {
                        // Primero se carga el fichero
                        file = await SaveImageAsync(filename => "ad_" + DateTime.Now.ToString("yyyyMMddHHssffff") + filename);
                        if (string.IsNullOrEmpty(file.Error))
                        {

                            // Si todo fue bien, se guarda la informacion de la url (absoluta) en el anuncio
                            Ads entity = new Ads
                            {
                                Order = model.Order,
                                Seconds = model.Seconds,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                ImageUrl = file.Url,
                            };

                            Db.Ads.Add(entity);
                            await Db.SaveChangesAsync();

                            result.Id = entity.Id;

                            return Ok(result);
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("API - AdsController", e);
                    }

                    // Si se llega hasta aqui, es que ha habido un error durante la ejecucion
                    return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
                else
                {
                    Logger.Warn("API - AdsController - Post - Validation Error: " + ModelState.GetFirstError());
                    return Error(HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
                }
            }
        }

        // PUT: /api/Ads/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Put([FromBody]AdModel model)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            if (model.Id <= 0)
                ModelState.AddModelError("", Resources.Resources.Common_ErrorMessages_InvalidOperation);

            if (ModelState.IsValid)
            {
                Ads entity = Db.Ads.SingleOrDefault(x => x.Id == model.Id);
                if (entity != null)
                {
                    entity.Order = model.Order;
                    entity.Seconds = model.Seconds;
                    entity.UpdateDate = DateTime.Now;
                    try
                    {
                        await Db.SaveChangesAsync();
                        return Ok(result);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("API - AdsController", e);
                        return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                    }
                }
                else
                {
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            else
            {
                Logger.Warn("API - AdsController - Put - Validation Error: " + ModelState.GetFirstError());
                return Error(HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        // PUT: /api/Ads/{id}/Recover
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/Recover")]
        public async Task<IHttpActionResult> Recover(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            try
            {
                Ads entity = Db.Ads.SingleOrDefault(x => x.Id == id && x.Deleted);
                if (entity != null)
                {
                    // El borrado es lógico
                    entity.Deleted = false;
                    entity.UpdateDate = DateTime.Now;
                    await Db.SaveChangesAsync();
                    return Ok(result);
                }
                else
                {
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            catch (Exception e)
            {
                Logger.Error("API - AdsController", e);
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }


        // PUT: /api/Ads/{id}/Image
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/image")]
        public async Task<IHttpActionResult> UploadImage(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var file = new FileUploadResponseModel();
            var result = new FileUploadResponseModel();

            try
            {
                // Se comprueba que existe el anuncio al que se va a asignar
                var ad = await Db.Ads.SingleOrDefaultAsync(x => x.Id == id);
                if (ad == null)
                    result.Error = Resources.Resources.Common_ErrorMessages_InvalidOperation;
                else
                {
                    // Se guarda el archivo en servidor.
                    file = await SaveImageAsync(filename => "ad_" + DateTime.Now.ToString("yyyyMMddHHssffff") + filename);
                    if (string.IsNullOrEmpty(file.Error))
                    {

                        string previousImageUrl = ad.ImageUrl;

                        // Si todo fue bien, se guarda la informacion de la url (absoluta) en la promocion
                        ad.ImageUrl = file.Url;
                        ad.UpdateDate = DateTime.Now;
                        await Db.SaveChangesAsync();

                        // Una vez hecho esto, se retorna la url de la imagen
                        result.Url = Url.Content(ad.ImageUrl);

                        // Si existía un fichero previo (con otro nombre), se trata de eliminar
                        if (!string.IsNullOrEmpty(previousImageUrl) && !string.Equals(ad.ImageUrl, previousImageUrl))
                        {
                            string previousImagePath = HttpContext.Current.Server.MapPath(previousImageUrl);
                            try
                            {
                                if (File.Exists(previousImagePath))
                                    File.Delete(previousImagePath);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("API - AdsController - No se pudo eliminar la imagen previa del anuncio #" + ad.Id + ": " + previousImagePath, e);
                            }
                        }

                        return Ok(result);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("AdsController - ERROR", e);
            }

            return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
        }

        // DELETE: /api/Ads/{id}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Delete(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();
            
            Ads entity = Db.Ads.SingleOrDefault(x => x.Id == id && !x.Deleted);
            if (entity != null)
            {
                try
                {
                    // El borrado es lógico
                    entity.Deleted = true;
                    entity.UpdateDate = DateTime.Now;
                    await Db.SaveChangesAsync();

                    return Ok(result);
                }
                catch (Exception e)
                {
                    Logger.Error("API - AdsController", e);
                    return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
            }
            else
            {
                return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
            }
        }
    }
}
