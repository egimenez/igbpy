﻿using Igbpy.ApiWeb.Models;
using Igbpy.ApiWeb.Models.Api;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Globalization;
using Microsoft.AspNet.Identity;

namespace Igbpy.ApiWeb.Api
{
    [IgbpyApiAuthorize(Roles = Domain.Shared.Globals.Roles.Admin)]
    [RoutePrefix("api/Generic")]
    public class GenericController : IgbpyApiController
    {


        #region Utils


        /// <summary>
        /// Mapea los elementos de una query a elementos de modelo manejables
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<AdModel> MapModel(IQueryable<Generic> query, HttpContext httpContext)
        {
            // Se prepara la query para obtener todos los datos en bruto en el objeto de modelo
            // Los datos que necesiten procesado posterior (fechas que convertir en string, urls que 
            // convertir en absolutas, etc) se almacenan separadamente
            var tempQuery = query.Select(x => new
            {
                Model = new AdModel
                {
                    // Se cargan todos los campos posibles, salvo aquellos que requieran procesado
                    Id = x.Id,

                    Deleted = x.Deleted,
                },
                // Ahora los valores que se necesiten procesar
            });

            // Se llama a ToArray() para que se ejecute la query y se tengan los datos cargados, se
            // hace un último select en el cual se cargan en el objeto de modelo los datos que requirieran procesado
            // y se retorna
            return tempQuery.ToArray().Select(x =>
            {

                // Para cada resultado, se devuelve un objeto de modelo con los datos procesados
                return x.Model;
            });
        }

        #endregion

        // GET: /api/Generic/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OverrideAuthorization]
        [IgbpyApiAuthorize]
        public async Task<IHttpActionResult> Get([FromUri]QueryRequestModel model)
        {
            QueryResponseModel<AdModel> result = null;
            HttpContext httpContext = HttpContext.Current;

            // Se obtienen los resultados
            try
            {
                result = await GetQueryResponseAsync(model.Offset, model.Size, () => Db.Generics.AsNoTracking().Where(x => model.IncludeDeleted || !x.Deleted).OrderBy(x => x.Id), query => MapModel(query, httpContext));

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("API - GenericController", e);
                result.Error = Resources.Resources.Common_ErrorMessages_QueryItemsFailure;
            }

            return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
        }

        // GET: /api/Generic/{id}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OverrideAuthorization]
        [IgbpyApiAuthorize]
        public async Task<IHttpActionResult> Get(int id)
        {
            QueryResponseModel<AdModel> result = null;
            HttpContext httpContext = HttpContext.Current;
            // Se obtienen el resultado
            try
            {
                result = await GetQueryResponseAsync(0, 1, () => Db.Generics.AsNoTracking().Where(x => x.Id == id), query => MapModel(query, httpContext));
                if (result.TotalSize != 1)
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("API - GenericController", e);
                result.Error = Resources.Resources.Common_ErrorMessages_QueryItemsFailure;
            }

            return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
        }

        // POST: /api/Generic/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Post([FromBody] GenericModel model)
        {
            var result = new CreateResponseModel<int>();

            // Si es válido, se continúa
            if (ModelState.IsValid)
            {
                try
                {
                    // Si todo fue bien, se guarda la informacion de la url (absoluta) en el anuncio
                    Generic entity = new Generic
                    {

                        String = model.String,
                        Integer = model.Integer,
                        Location = model.Location,

                        Boolean = model.Boolean,

                        //Cuando recibis fecha y hora (para recibir solo fecha reemplazar "d/MM/yyyy HH:mm" por la cadena "d/MM/yyyy")
                        DateTime = DateTime.ParseExact(model.DateTime, "d/MM/yyyy HH:mm", CultureInfo.InvariantCulture),

                        CreateUserId = User.Identity.GetUserId(),
                        UpdateUserId = User.Identity.GetUserId(),
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                    };

                    Db.Generics.Add(entity);
                    await Db.SaveChangesAsync();

                    result.Id = entity.Id;

                    return Ok(result);
                }
                catch (Exception e)
                {
                    Logger.Error("API - GenericController", e);
                }

                // Si se llega hasta aqui, es que ha habido un error durante la ejecucion
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
            else
            {
                Logger.Warn("API - GenericController - Post - Validation Error: " + ModelState.GetFirstError());
                return Error(HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        // PUT: /api/Generic/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Put([FromBody]GenericModel model)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            if (model.Id <= 0)
                ModelState.AddModelError("", Resources.Resources.Common_ErrorMessages_InvalidOperation);

            if (ModelState.IsValid)
            {
                Generic entity = Db.Generics.SingleOrDefault(x => x.Id == model.Id);
                if (entity != null)
                {
                    entity.String = model.String;
                    entity.Integer = model.Integer;
                    entity.Location = model.Location;

                    entity.Boolean = model.Boolean;

                    //Cuando recibis fecha y hora (para recibir solo fecha reemplazar "d/MM/yyyy HH:mm" por la cadena "d/MM/yyyy")
                    entity.DateTime = DateTime.ParseExact(model.Date, "d/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                    entity.CreateUserId = User.Identity.GetUserId();
                    entity.UpdateUserId = User.Identity.GetUserId();

                    entity.CreateDate = DateTime.Now;
                    entity.UpdateDate = DateTime.Now;

                    try
                    {
                        await Db.SaveChangesAsync();
                        return Ok(result);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("API - GenericController", e);
                        return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                    }
                }
                else
                {
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            else
            {
                Logger.Warn("API - GenericController - Put - Validation Error: " + ModelState.GetFirstError());
                return Error(HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        // PUT: /api/Generic/{id}/Recover
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/Recover")]
        public async Task<IHttpActionResult> Recover(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            try
            {
                Generic entity = Db.Generics.SingleOrDefault(x => x.Id == id && x.Deleted);
                if (entity != null)
                {
                    // El borrado es lógico
                    entity.Deleted = false;
                    entity.UpdateDate = DateTime.Now;
                    await Db.SaveChangesAsync();
                    return Ok(result);
                }
                else
                {
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            catch (Exception e)
            {
                Logger.Error("API - GenericController", e);
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }


        // DELETE: /api/Generic/{id}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Delete(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            Generic entity = Db.Generics.SingleOrDefault(x => x.Id == id && !x.Deleted);
            if (entity != null)
            {
                try
                {
                    // El borrado es lógico
                    entity.Deleted = true;
                    entity.UpdateDate = DateTime.Now;
                    await Db.SaveChangesAsync();

                    return Ok(result);
                }
                catch (Exception e)
                {
                    Logger.Error("API - GenericController", e);
                    return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
            }
            else
            {
                return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
            }
        }
    }
}
