﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Igbpy.ApiWeb.Models;
using Igbpy.ApiWeb.Results;
using Igbpy.Domain.IdentityModels;
using System.Linq;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Models;
using System.Data.Entity;
using PushSharp.Google;
using PushSharp.Core;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Igbpy.Domain.Extensions.Settings;
using PushSharp.Apple;
using Igbpy.ApiWeb.Models.Api;

namespace Igbpy.ApiWeb.Api
{
    [IgbpyApiAuthorize(Roles = Domain.Shared.Globals.Roles.Admin)]
    [RoutePrefix("api/CloudMsg")]
    public class CloudMsgController : IgbpyApiController
    {
        
        public async Task<IHttpActionResult> Get([FromUri]SentCloudMsgQueryRequestModel model)
        {
            QueryResponseModel<SentCloudMsgModel> result = null;

            // Se prepara el resultado
            try
            {
                result = await GetQueryResponseAsync(model.Offset, model.Size,
                    () =>
                    {
                        IQueryable<SentCloudMsg> query = Db.SentCloudMsgs.AsNoTracking().Include("User");

                        // Filtros
                        query = query.Where(x => !x.Deleted);
                        if (model.MessageType > 0)
                            query = query.Where(x => x.MessageType == model.MessageType);

                        // Ordenacion
                        query = query.OrderByDescending(x => x.SentDate);

                        // Este tipo de elemento tiene un limite bien marcado de máximo de elementos (porque crece deprisa)
                        return query.Take(100);
                    },
                    query =>
                    {
                        return query.Select(x => new
                        {
                            Model = new SentCloudMsgModel
                            {
                                Id = x.Id,
                                MessageType = x.MessageType,
                                Message = x.Message,
                                User = new SentCloudMsgModel.UserModel
                                {
                                    Id = x.UserId,
                                    UserName = x.User.UserName,
                                },
                            },
                            SentDate = x.SentDate,
                            ExtraData = x.ExtraData,
                        }).ToArray().Select(x =>
                        {
                            x.Model.SentDate = x.SentDate.ToString("dd/MM/yyyy, HH:mm:ss");
                            x.Model.ExtraData = JsonConvert.DeserializeObject<JObject>(x.ExtraData);
                            return x.Model;
                        });
                    });

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("API - CloudMsgController", e);
                return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }

        // POST: /api/CloudMsg/Add
        [AllowAnonymous]
        [HttpPost]
        [Route("Add")]
        public async Task<IHttpActionResult> Add([FromBody]CloudMsgTokenModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string userId = User.Identity.IsAuthenticated ? User.Identity.GetUserId() : null;
                    var entity = await Db.CloudMsgToken.FirstOrDefaultAsync(x => x.Token == model.Token);
                    if (entity == null)
                    {
                        entity = new CloudMsgToken
                        {
                            Token = model.Token,
                            Platform = model.Platform,
                            UserId = userId,
                        };
                        Db.CloudMsgToken.Add(entity);
                    }
                    else if (userId != null)
                    {
                        entity.UserId = userId;
                    }
                    entity.UpdateDate = DateTime.Now;

                    await Db.SaveChangesAsync();

                    return Ok();
                }
                catch (Exception ex)
                {
                    Logger.Error("API - CloudMsgController", ex);
                    return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
            }
            else
            {
                Logger.Warn("API - CloudMsgController - Add - ValidationError: " + ModelState.GetFirstError());
                return Error(System.Net.HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        [HttpPost]
        [Route("SendBroadcastPush", Name = "SendBroadcastPushApi")]
        public async Task<IHttpActionResult> SendBroadcastPushNotification([FromBody]CloudMsgModel model)
        {
            var result = new ResponseModel();
            string userId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                try
                {
                    // Se envía a todos los registrados un mensaje sin informacion adicional
                    var tokens = (await Db.CloudMsgToken.AsNoTracking().ToListAsync());
                    await Helpers.SendMessage(model.Message, Globals.CloudMessage.PushMessageType.Message, null, tokens, userId, Logger);
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error("API - CloudMsgController", ex);
                    return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
            }
            else
            {
                Logger.Warn("API - CloudMsgController - SendBroadcastPushNotification - ValidationError: " + ModelState.GetFirstError());
                return Error(System.Net.HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        [HttpPost]
        [Route("SendPush", Name = "SendPushApi")]
        public async Task<IHttpActionResult> SendPushNotification([FromBody]CloudMsgModel model)
        {
            var result = new ResponseModel();
            string userId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                try
                {
                    if (model.ReceivingTokensIds.Count() > 0)
                    {
                        // Se obtienen los tokens apropiados de la base de datos agrupados por dispositivo
                        var tokens = (await Db.CloudMsgToken.AsNoTracking().Where(x => model.ReceivingTokensIds.Contains(x.Id)).ToListAsync());
                        await Helpers.SendMessage(model.Message, Globals.CloudMessage.PushMessageType.Message, null, tokens, userId, Logger);
                    }
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    Logger.Error("API - CloudMsgController", ex);
                    return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
            }
            else
            {
                Logger.Warn("API - CloudMsgController - SendPushNotification - ValidationError: " + ModelState.GetFirstError());
                return Error(System.Net.HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        [HttpPost]
        [Route("SendCampaignPush", Name = "SendCampaignPushApi")]
        public async Task<IHttpActionResult> SendCampaignPushNotification([FromUri]int id)
        {
            var result = new ResponseModel();
            string userId = User.Identity.GetUserId();

            try
            {
                // Se comprueba que se haya pasado un mensaje válido
                var entity = await Db.Campaigns.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id && !x.Deleted);
                if (entity == null)
                    return Error(System.Net.HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                else
                {
                    // En otro caso, se obtienen todos los tokens para Mensajes Push disponibles
                    // agrupados por plataforma
                    await Helpers.SendMessage(entity.Description, Globals.CloudMessage.PushMessageType.Campaign, new { Id = id, ImageUrl = Url.Content(entity.ImageUrl), WebUrl = entity.WebUrl }, await Db.CloudMsgToken.AsNoTracking().ToListAsync(), userId, Logger);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("API - CloudMsgController", ex);
                return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }
    }
}