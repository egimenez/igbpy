﻿using Igbpy.ApiWeb.Models;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Resources;
using Igbpy.ApiWeb.Models.Api;

namespace Igbpy.ApiWeb.Api
{
    [IgbpyApiAuthorize(Roles = Domain.Shared.Globals.Roles.Admin)]
    [RoutePrefix("api/Campaigns")]
    public class CampaignsController : IgbpyApiController
    {


        #region Utils

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetNewCampaignFileName(string filename)
        {
            return "campaign_" + DateTime.Now.ToString("yyyyMMddHHssffff") + filename;
        }

        /// <summary>
        /// Mapea un conjunto de resultados a elementos de modelo correctos
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private IEnumerable<CampaignModel> MapModel(IQueryable<Campaign> query, HttpContext httpContext)
        {
            // Se prepara la query para obtener todos los datos en bruto en el objeto de modelo
            // Los datos que necesiten procesado posterior (fechas que convertir en string, urls que 
            // convertir en absolutas, etc) se almacenan separadamente
            var tempQuery = query.Select(x => new
            {
                Model = new CampaignModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    //ImageUrl = x.ImageUrl,
                    //ImageThumbUrl
                    WebUrl = x.WebUrl,
                    Deleted = x.Deleted,
                },
                // Ahora los valores que se necesiten procesar
                ImageUrl = x.ImageUrl
            });

            // Se llama a ToArray() para que se ejecute la query y se tengan los datos cargados, se
            // hace un último select en el cual se cargan en el objeto de modelo los datos que requirieran procesado
            // y se retorna

            return tempQuery.ToArray().Select(x =>
            {
                // Si hay una url cargada, se procesa correctamente y se guarda en el objeto de modelo
                if (!string.IsNullOrEmpty(x.ImageUrl))
                {
                    x.Model.ImageUrl = Url.Content(x.ImageUrl);
                    string imageThumbUrl = Helpers.GetThumbnailUrl(x.ImageUrl, httpContext, true);
                    if (imageThumbUrl != null)
                        x.Model.ImageThumbUrl = Url.Content(imageThumbUrl);
                }
                // Para cada resultado, se devuelve un objeto de modelo con los datos procesados
                return x.Model;
            });
        }

        #endregion

        // GET: /api/Campaigns/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OverrideAuthorization]
        [IgbpyApiAuthorize]
        public async Task<IHttpActionResult> Get([FromUri]QueryRequestModel model)
        {
            QueryResponseModel<CampaignModel> result = null;

            // Se prepara el resultado
            try
            {
                HttpContext httpContext = HttpContext.Current;
                result = await GetQueryResponseAsync(model.Offset, model.Size,
                    () => Db.Campaigns.AsNoTracking().Where(x => model.IncludeDeleted || !x.Deleted).OrderBy(x => x.Name),
                    query => MapModel(query, httpContext));

                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("API - CampaignsController", e);
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }

        }

        // GET: /api/Campaigns/{id}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OverrideAuthorization]
        [IgbpyApiAuthorize]
        public async Task<IHttpActionResult> Get(int id)
        {
            QueryResponseModel<CampaignModel> result = null;
            try
            {
                HttpContext httpContext = HttpContext.Current;
                result = await GetQueryResponseAsync(0, 1,
                    () => Db.Campaigns.AsNoTracking().Where(x => x.Id == id),
                    query => MapModel(query, httpContext));

                if (result.TotalSize != 1)
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);

                return Ok(result.Items.SingleOrDefault());
            }
            catch (Exception e)
            {
                Logger.Error("API - CampaignsController", e);
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }

        // POST: /api/Campaigns/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Post()
        {
            var result = new CreateResponseModel<int>();
            var model = new CampaignModel();
            var file = new FileUploadResponseModel();
            var request = HttpContext.Current.Request;

            // Debe enviarse como multipart, porque debe incluir el fichero
            if (!Request.Content.IsMimeMultipartContent())
            {
                return Error(HttpStatusCode.UnsupportedMediaType, Resources.Resources.Common_ErrorMessages_InvalidOperation);
            }
            else
            {
                // Se obtienen los valores del nuevo elemento de parametros de request
                model.Name = request.Params["Name"];
                model.Description = request.Params["Description"];
                model.WebUrl = request.Params["WebUrl"];

                // Se validan los parametros
                if (string.IsNullOrWhiteSpace(model.Name))
                    ModelState.AddModelError("Name", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Campaigns_Fields_Name));
                if (string.IsNullOrWhiteSpace(model.Name))
                    ModelState.AddModelError("Description", string.Format(Resources.Resources.Common_ValidationError_Required, Resources.Resources.Admin_Campaigns_Fields_Description));
                if (string.IsNullOrWhiteSpace(model.WebUrl))
                    model.WebUrl = null;

                // Si es válido, se continúa
                if (ModelState.IsValid)
                {
                    try
                    {
                        // Primero se carga el fichero
                        file = await SaveImageAsync(filename => "campaigns_" + DateTime.Now.ToString("yyyyMMddHHssffff") + filename);
                        if (string.IsNullOrEmpty(file.Error))
                        {

                            // Si todo fue bien, se guarda la informacion de la url (absoluta) en la campaña
                            Campaign entity = new Campaign
                            {
                                Name = model.Name,
                                Description = model.Description,
                                WebUrl = model.WebUrl,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                ImageUrl = file.Url,
                            };

                            Db.Campaigns.Add(entity);
                            await Db.SaveChangesAsync();

                            result.Id = entity.Id;

                            return Ok(result);
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("API - CampaignsController", e);
                    }

                    // Si se ha llegado hasta aqui, es qeu ha habido algún error
                    return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
                else
                {
                    Logger.Warn("API - CampaignController - Post - Validation Error: " + ModelState.GetFirstError());
                    return Error(HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
                }
            }

        }

        // PUT: /api/Campaigns/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Put([FromBody]CampaignModel model)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            if (ModelState.IsValid)
            {
                Campaign entity = Db.Campaigns.SingleOrDefault(x => x.Id == model.Id);
                if (entity != null)
                {
                    entity.Name = model.Name;
                    entity.Description = model.Description;
                    entity.WebUrl = model.WebUrl;
                    entity.UpdateDate = DateTime.Now;
                    try
                    {
                        await Db.SaveChangesAsync();
                        return Ok(result);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("API - CampaignsController", e);
                        return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                    }
                }
                else
                {
                    Logger.Warn("API - CampaignController - Post - Validation Error: " + ModelState.GetFirstError());
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            else
            {
                return Error(HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        // PUT: /api/Ads/{id}/Recover
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/Recover")]
        public async Task<IHttpActionResult> Recover(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            try
            {
                Campaign entity = Db.Campaigns.SingleOrDefault(x => x.Id == id && x.Deleted);
                if (entity != null)
                {

                    // El borrado es lógico
                    entity.Deleted = false;
                    entity.UpdateDate = DateTime.Now;
                    await Db.SaveChangesAsync();
                    return Ok(result);
                }
                else
                {
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            catch (Exception e)
            {
                Logger.Error("API - CampaignsController", e);
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }

        }

        // PUT: /api/Campaigns/{id}/Image
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/image")]
        public async Task<IHttpActionResult> UploadImage(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var file = new FileUploadResponseModel();
            var result = new FileUploadResponseModel();

            try
            {

                // Se comprueba que existe la campaña al que se va a asignar
                var ad = await Db.Campaigns.SingleOrDefaultAsync(x => x.Id == id);
                if (ad == null)
                    result.Error = Resources.Resources.Common_ErrorMessages_InvalidOperation;
                else
                {
                    // Se guarda el archivo en servidor.
                    file = await SaveImageAsync(filename => "campaign_" + DateTime.Now.ToString("yyyyMMddHHssffff") + filename);
                    if (string.IsNullOrEmpty(file.Error))
                    {

                        string previousImageUrl = ad.ImageUrl;

                        // Si todo fue bien, se guarda la informacion de la url (absoluta) en la promocion
                        ad.ImageUrl = file.Url;
                        ad.UpdateDate = DateTime.Now;
                        await Db.SaveChangesAsync();

                        // Una vez hecho esto, se retorna la url de la imagen
                        result.Url = Url.Content(ad.ImageUrl);

                        // Si existía un fichero previo (con otro nombre), se trata de eliminar
                        if (!string.IsNullOrEmpty(previousImageUrl) && !string.Equals(ad.ImageUrl, previousImageUrl))
                        {
                            string previousImagePath = HttpContext.Current.Server.MapPath(previousImageUrl);
                            try
                            {
                                if (File.Exists(previousImagePath))
                                    File.Delete(previousImagePath);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("API - CampaignsController - No se pudo eliminar la imagen previa de la campaña #" + ad.Id + ": " + previousImagePath, e);
                            }
                        }

                        return Ok(result);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("CampaignsController - ERROR", e);
            }

            // Si se llega aqui, se trata de un error
            return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
        }

        // DELETE: /api/Campaigns/{id}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Delete(int id)
        {
            // Valida el modelo y guarda el item en base de datos
            var result = new ResponseModel();

            try
            {
                Campaign entity = Db.Campaigns.SingleOrDefault(x => x.Id == id && !x.Deleted);
                if (entity != null)
                {
                    // El borrado es lógico
                    entity.Deleted = true;
                    entity.UpdateDate = DateTime.Now;
                    await Db.SaveChangesAsync();
                    return Ok(result);
                }
                else
                {
                    return Error(HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            catch (Exception e)
            {
                Logger.Error("API - CampaignsController", e);
                return Error(HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }
    }
}
