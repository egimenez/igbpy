﻿using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Igbpy.ApiWeb.Api
{
    /// <summary>
    /// Controlador para llevar a cabo la sincronización de datos entre una App y el servidor
    /// </summary>
    public class SincroController : IgbpyApiController
    {
        /// <summary>
        /// Obtiene todos los datos relevantes del sistema, o bien aquellos que se hayan actualizado
        /// a partir de una cierta marca de tiempo-
        /// </summary>
        /// <param name="lastUpdate">Marca de última actualización. Si se introduce, se devolverán sólo los elementos que
        /// se hayan actualizado a partir de la fecha que representa (incluidos los eliminados lógicos), en otro caso,
        /// devuelve todos los elementos no eliminados.</param>
        /// <returns>un objeto JSON con una propiedad por cada colección de elementos, y un campo "lastUpdate" (texto)
        /// con la marca de fecha a enviar en la siguiente llamada para obtener sólo los actualizados desde entonces.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(string lastUpdate = null)
        {
            DateTime? dtTimestamp = null;

            // se guarda el momento actual como la fecha de ultima actualizacion por si acaso se
            // introdujeran datos antes de finalizar que no se retornaran en esta llamada, de modo que lo 
            // hagan en la proxima
            DateTime now = DateTime.Now;

            // Si se ha introducido una marca de ultima actualización, se obtiene la fecha apropiada
            if (lastUpdate != null)
                dtTimestamp = getLastUpdateFromTicks(lastUpdate);

            // Se realizan las consultas para todos los datos a retornar

            // - Anuncios
            IQueryable<Ads> adsQuery = Db.Ads.AsNoTracking();

            // Si hay marca de ultima actualizacion, se devuelven todos los actualizados desde entonces (no incluidos)
            if (dtTimestamp != null)
                adsQuery = adsQuery.Where(x => x.UpdateDate > dtTimestamp);
            // Si no, se devuelven todos los que no estén borrados
            else
                adsQuery = adsQuery.Where(x => !x.Deleted);

            var ads = (await adsQuery.ToListAsync()).Select(x => new {
                Id = x.Id,
                ImageUrl = !string.IsNullOrEmpty(x.ImageUrl) ? Url.Content(x.ImageUrl) : null,
                Order = x.Order,
                Seconds = x.Seconds,
                Deleted = x.Deleted
            });

            // - INFO: Incluir los necesarios

            // Retorna un objeto apropiado con todos los datos 
            return Ok(new
            {
                // Las distintas colecciones de elementos
                ads = ads,
                // La fecha de última actualización para futuras llamadas
                lastUpdate = now.Ticks.ToString()
            });
        }


        #region Utils

        /// <summary>
        /// Obtiene un objeto de tipo DateTime a partir de la cadena de caracteres de la marca de fecha.
        /// </summary>
        /// <param name="strTicks"></param>
        /// <returns></returns>
        private DateTime getLastUpdateFromTicks(String strTicks)
        {
            long ticks;
            if (!string.IsNullOrEmpty(strTicks))
                if (strTicks.Contains('.'))
                    strTicks = strTicks.Remove(strTicks.IndexOf('.'), strTicks.Length - strTicks.IndexOf('.'));
            if (!string.IsNullOrWhiteSpace(strTicks) && Int64.TryParse(strTicks, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out ticks))
                return new DateTime(ticks);

            return new DateTime(1900, 1, 1);
        } 

        #endregion


    }
}
