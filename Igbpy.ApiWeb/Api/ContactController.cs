﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain.Models;
using System.Data.Entity;
using Igbpy.ApiWeb.Models;
using System.Linq;
using Igbpy.ApiWeb.Models.Api;

namespace Igbpy.ApiWeb.Api
{
    [IgbpyApiAuthorize(Roles = Domain.Shared.Globals.Roles.Admin)]
    public class ContactController : IgbpyApiController
    {
        // GET: /api/Contact/
        public async Task<IHttpActionResult> Get([FromUri]QueryRequestModel model)
        {
            QueryResponseModel<ContactModel> result = null;

            try
            {
                result = await GetQueryResponseAsync(model.Offset, model.Size,
                    () => Db.Contacts.AsNoTracking().Where(x => !x.Deleted).OrderByDescending(x => x.CreateDate),
                    query =>
                    {
                        return query.Select(x => new
                        {
                            Model = new ContactModel
                            {
                                Id = x.Id,
                                Message = x.Message,
                                User = x.User != null ? new ContactModel.UserModel { Id = x.User.Id, UserName = x.User.UserName } : null,
                            },
                            CreateDate = x.CreateDate,
                        }).ToArray().Select(x =>
                        {
                            x.Model.CreateDate = x.CreateDate.ToString("dd/MM/yyyy HH:mm");
                            return x.Model;
                        });
                    });
                return Ok(result);
            }
            catch (Exception e)
            {
                Logger.Error("Api - ContactController", e);
                return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/Contact/Add")]
        public async Task<IHttpActionResult> Add([FromBody]ContactModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Crea la nueva entidad
                    var entity = new Contact
                    {
                        Message = model.Message,
                        UserId = User.Identity.GetUserId(),
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                    };
                    Db.Contacts.Add(entity);

                    await Db.SaveChangesAsync();
                    return Ok(new CreateResponseModel<int>{ Id = entity.Id });
                }
                catch (Exception ex)
                {
                    Logger.Error("API - ContactController", ex);
                    return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
                }
            }
            else
            {
                Logger.Warn("API - ContactController - Add - Validation Error: " + ModelState.GetFirstError());
                return Error(System.Net.HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_ValidationError);
            }
        }

        // DELETE: /api/Contact/{id}
        public async Task<IHttpActionResult> Delete(int id)
        {
            var result = new ResponseModel();

            try
            {
                var entity = await Db.Contacts.SingleOrDefaultAsync(x => x.Id == id && !x.Deleted);
                if (entity != null)
                {

                    entity.Deleted = true;
                    await Db.SaveChangesAsync();
                    return Ok(result);
                }
                else
                {
                    return Error(System.Net.HttpStatusCode.Forbidden, Resources.Resources.Common_ErrorMessages_InvalidOperation);
                }
            }
            catch (Exception e)
            {
                Logger.Error("API - ContactController", e);
                return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);
            }

        }
    }
}