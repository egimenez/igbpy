﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Igbpy.ApiWeb.Models;
using Igbpy.ApiWeb.Results;
using Igbpy.Domain.IdentityModels;
using System.Linq;
using Igbpy.ApiWeb.Shared;
using Igbpy.Domain;
using Igbpy.ApiWeb.Models.Api;

namespace Igbpy.ApiWeb.Api.Controllers
{
    /// <summary>
    /// Este controlador está heredado del template base para aplicaciones de Web Api de microsoft. Utilizar y modificar con cuidado.
    /// </summary>
    [IgbpyApiAuthorize]
    [RoutePrefix("api/Account")]
    public class AccountController : IgbpyApiController
    {
        private const string LocalLoginProvider = "Local";

        public AccountController()
        {
        }

        public AccountController(ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            AccessTokenFormat = accessTokenFormat;
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        #region Util
        public async Task<User> FindAsync(UserLoginInfo loginInfo)
        {
            User user = await UserManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(User user)
        {
            var result = await UserManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await UserManager.AddLoginAsync(userId, login);

            return result;
        }
        #endregion


        [Route("GetUserInfo")]
        public async Task<IHttpActionResult> GetUserInfo()
        {
            var userId = User.Identity.GetUserId();
            var user = await UserManager.FindByIdAsync(userId);

            if (user != null)
            {
                return Ok(new
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Email = user.Email,
                    Name = user.Name,
                    PhoneNumber = user.PhoneNumber
                });
            }
            else
            {
                Authentication.SignOut();
                return Unauthorized();
            }
        }

        [Route("Update")]
        [HttpPost]
        public async Task<IHttpActionResult> Update(RegisterBindingModel model)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                Authentication.SignOut();
                return Unauthorized();
            }

            user.UserName = model.Email;
            user.Email = model.Email;
            user.Name = model.Name;
            user.PhoneNumber = model.PhoneNumber;

            var updateResult = UserManager.Update(user);

            if (updateResult.Succeeded)
            {
                return Ok(new ResponseModel());
            }
            else
            {
                return GetErrorResult(updateResult);
            }

        }

        //// GET api/Account/UserInfo
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("UserInfo")]
        //public UserInfoViewModel GetUserInfo()
        //{
        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    return new UserInfoViewModel
        //    {
        //        Email = User.Identity.GetUserName(),
        //        HasRegistered = externalLogin == null,
        //        LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
        //    };
        //}

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(Authentication.GetAuthenticationTypes().Select(x => x.AuthenticationType).ToArray());
            return Ok(new ResponseModel());
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        //[AllowAnonymous]
        //[Route("ExternalLoginTest")]
        //[HttpGet]
        //public IHttpActionResult ExternalLoginTest(string provider)
        //{
        //    var externalAuthenticationType = Authentication.GetExternalAuthenticationTypes().SingleOrDefault(x => x.AuthenticationType == provider);
        //    if (externalAuthenticationType != null)
        //    {

        //        if (User.Identity.IsAuthenticated)
        //            Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

        //        var ctx = Request.GetOwinContext();
        //        ctx.Authentication.Challenge(
        //            new AuthenticationProperties
        //            {
        //                RedirectUri = Url.Content(Url.Route("DefaultApi", new { controller = "Account" }) + "/ExternalLoginCallbackTest?provider=" + externalAuthenticationType.AuthenticationType)
        //            },
        //            externalAuthenticationType.AuthenticationType);
        //        return Unauthorized();
        //    }
        //    else
        //        return NotFound();
        //}

        ////[OverrideAuthorization]
        ////[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        //[AllowAnonymous]
        //[Route("ExternalLoginCallbackTest")]
        //[HttpGet]
        //public async Task<IHttpActionResult> ExternalLoginCallbackTest(string provider)
        //{
        //    var result = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ExternalCookie);
        //    if (result != null)
        //    {
        //        Claim providerKeyClaim = result.Identity.FindFirst(ClaimTypes.NameIdentifier);
        //        if (providerKeyClaim == null)
        //            return InternalServerError();

        //        string identityProvider = providerKeyClaim.Issuer,
        //            providerKey = providerKeyClaim.Value;

        //        if (provider != identityProvider)
        //        {
        //            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //            return Redirect(Url.Route("DefaultApi", new { controller = "Account" }) + "/ExternalLoginTest?provider=" + provider);
        //        }

        //        ExternalLoginInfo externalLoginInfo = new ExternalLoginInfo
        //        {
        //            DefaultUserName = result.Identity.FindFirstValue(ClaimTypes.Email),
        //            Email = result.Identity.FindFirstValue(ClaimTypes.Email),
        //            ExternalIdentity = result.Identity,
        //            Login = new UserLoginInfo(identityProvider, providerKey)
        //        };

        //        // Hace el login
        //        var signInResult = SignInManager.ExternalSignIn(externalLoginInfo, true);
        //        switch (signInResult)
        //        {
        //            case SignInStatus.Success:
        //                return RedirectToRoute("Default", new { controller = "Home", action = "Index" });
        //            //break;
        //            case SignInStatus.LockedOut:
        //                return NotFound();
        //            case SignInStatus.RequiresVerification:
        //                return InternalServerError();
        //            case SignInStatus.Failure:
        //            default:
        //                // Se registra el nuevo user y se hace login
        //                User newUser = new User
        //                {
        //                    UserName = externalLoginInfo.DefaultUserName,
        //                    Email = externalLoginInfo.Email,
        //                    Platform = Domain.Shared.Globals.Platforms.Web,
        //                   // Se pueden agregar más valores de los Claims  
        //                };

        //                var createResult = await UserManager.CreateAsync(newUser);
        //                if (createResult.Succeeded)
        //                {
        //                    createResult = await UserManager.AddLoginAsync(newUser.Id, externalLoginInfo.Login);
        //                    if (createResult.Succeeded)
        //                    {
        //                        return Redirect(Url.Route("DefaultApi", new { controller = "Account" }) + "/ExternalLoginCallbackTest?provider=" + providerKey);
        //                    }
        //                }
        //                break;
        //        }
        //    }
            
        //    return Ok(new { algo = "No" });
        //}

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ClaimsIdentity externalIdentity = User.Identity as ClaimsIdentity;
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(externalIdentity);
            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            UserLoginInfo loginInfo = new UserLoginInfo(externalLogin.LoginProvider, externalLogin.ProviderKey);
            ExternalLoginInfo externalLoginInfo = new ExternalLoginInfo()
            {
                DefaultUserName = externalLogin.Email,
                ExternalIdentity = externalIdentity,
                Login = loginInfo,
            };

            var queryParams = Request.GetQueryNameValuePairs();
            string returnUrl = queryParams.Where(x => x.Key == "return_url").Select(x => x.Value).SingleOrDefault();

            var loginResult = await SignInManager.ExternalSignInAsync(externalLoginInfo, false);
            switch (loginResult)
            {
                case SignInStatus.Success:
                    return Ok();
                //break;
                case SignInStatus.LockedOut:
                    return NotFound();
                case SignInStatus.RequiresVerification:
                    return InternalServerError();
                case SignInStatus.Failure:
                default:
                    // Se registra el nuevo user y se hace login
                    User newUser = new User
                    {
                        UserName = externalLogin.Email,
                        Email = externalLogin.Email,
                    };

                    var createResult = await UserManager.CreateAsync(newUser);
                    if (createResult.Succeeded)
                    {
                        createResult = await UserManager.AddLoginAsync(newUser.Id, loginInfo);
                        if (createResult.Succeeded)
                        {
                            return Ok();
                        }
                    }
                    break;
            }

            return InternalServerError();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                string errores = "";
                foreach (var item in ModelState.Values)
                    foreach (var error in item.Errors)
                        errores += error.ErrorMessage + "\n";

                return BadRequest(errores);

            }

            var user = new User()
            {
                UserName = model.Email,
                Email = model.Email,
                Name = model.Name,
                Imei = model.Imei,
                Platform = Domain.Shared.Globals.Platforms.Mobile,
                PhoneNumber = model.PhoneNumber

            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new User() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string Email { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (Email != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, Email, null, LoginProvider));
                    claims.Add(new Claim(ClaimTypes.Email, Email, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    Email = identity.FindFirstValue(ClaimTypes.Email)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
