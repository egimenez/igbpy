﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Igbpy.Domain;
using Igbpy.Domain.Extensions.Settings;
using Igbpy.Domain.Models;
using System.Web.Http.ModelBinding;
using System.IO;
using System.Drawing;
using Igbpy.ApiWeb.Models.Api;
using System.Globalization;
using System.Configuration;

namespace Igbpy.ApiWeb.Shared
{
    public static class Helpers
    {
        #region ModelState
        public static string GetFirstError(this ModelStateDictionary modelState)
        {
            // Retorna el primero error (si encuentra) de un estado de modelo
            return modelState.Where(x => x.Value.Errors.Any()).Select(x => x.Value.Errors.Select(y => y.ErrorMessage).FirstOrDefault()).FirstOrDefault();
        }
        #endregion

        #region Default Culture and TimeZone

        /// <summary>
        /// Obtiene la cultura a utilizar por la aplicacion, que puede definirse en las configuraciones del proyecto
        /// </summary>
        /// <returns>El CultureInfo a utilizar en las distintas secciones en que sea necesaria la localizacion.</returns>
        public static CultureInfo GetDefaultCulture()
        {
            CultureInfo result = CultureInfo.CurrentCulture;
            try
            {
                string defaultCulture = ConfigurationManager.AppSettings[Globals.AppSettings.DefaultCulture];
                if (!string.IsNullOrWhiteSpace(defaultCulture))
                    result = CultureInfo.GetCultureInfo(defaultCulture);
            }
            catch (Exception)
            {
                // No hace nada
            }
            return result;
        }

        /// <summary>
        /// Obtiene la zona horario a utilizar por la aplicacion, que puede definirse en las configuraciones del proyecto
        /// </summary>
        /// <returns>El TimeZoneInfo a utilizar en las distintas secciones en que sea necesaria la localizacion.</returns>
        public static TimeZoneInfo GetDefaultTimeZoneInfo()
        {
            TimeZoneInfo result = TimeZoneInfo.Local;
            try
            {
                string defaultTimeZone = ConfigurationManager.AppSettings[Globals.AppSettings.DefaultTimeZone];
                if (!string.IsNullOrWhiteSpace(defaultTimeZone))
                    result = TimeZoneInfo.FindSystemTimeZoneById(defaultTimeZone);
            }
            catch (Exception)
            {
                // No hace nada
            }
            return result;
        }

        #endregion

        #region Conversiones de DateTime

        /// <summary>
        /// Convierte una fecha que se encuentra en la zona horaria local (del servidor) o UTC a la zona horaria por defecto
        /// </summary>
        /// <param name="datetime">Fecha</param>
        /// <returns></returns>
        public static DateTime ToIgbpyDateTime(this DateTime datetime)
        {
            if (datetime.Kind == DateTimeKind.Utc)
                return TimeZoneInfo.ConvertTimeFromUtc(datetime, GetDefaultTimeZoneInfo());
            return TimeZoneInfo.ConvertTime(datetime, GetDefaultTimeZoneInfo());
        }

        public static DateTime? ToIgbpyDateTime(this DateTime? datetime)
        {
            if (datetime.HasValue)
                return datetime.Value.ToIgbpyDateTime();
            return null;
        }

        /// <summary>
        /// Retorna la fecha en formato "d \de MMMM, yyyy"
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns>La fecha en formato "d \de MMMM, yyyy"</returns>
        public static string ToIgbpyLocaleDateString(this DateTime datetime, bool convertToDefaultCulture = true, bool convertToDefaultTimezone = true)
        {
            if (convertToDefaultTimezone)
                datetime = datetime.ToIgbpyDateTime();
            return datetime.ToString(@"d \de MMMM, yyyy", convertToDefaultCulture ? GetDefaultCulture() : CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Retorna la fecha en formato "d \de MMMM, yyyy" (o null)
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns>La fecha en formato "d \de MMMM, yyyy" (o null)</returns>
        public static string ToIgbpyLocaleDateString(this DateTime? datetime, bool convertToDefaultCulture = true, bool convertToDefaultTimezone = true)
        {
            return datetime?.ToIgbpyDateString(convertToDefaultTimezone);
        }

        /// <summary>
        /// Retorna la fecha en formato "dd/MM/yyyy"
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns>La fecha en formato "dd/MM/yyyy"</returns>
        public static string ToIgbpyDateString(this DateTime datetime, bool convertToDefaultTimezone = true)
        {
            if (convertToDefaultTimezone)
                datetime = datetime.ToIgbpyDateTime();
            return datetime.ToString("dd/MM/yyyy");
        }

        /// <summary>
        /// Retorna la fecha en formato "dd/MM/yyyy" (o null)
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns>La fecha en formato "dd/MM/yyyy" (o null)</returns>
        public static string ToIgbpyDateString(this DateTime? datetime, bool convertToDefaultTimezone = true)
        {
            return datetime?.ToIgbpyDateString(convertToDefaultTimezone);
        }


        /// <summary>
        /// Retorna la fecha en formato "dd/MM/yyyy HH:mm t"
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns>La fecha en formato "dd/MM/yyyy"</returns>
        public static string ToIgbpyDateTimeString(this DateTime datetime, bool convertToDefaultTimezone = true)
        {
            if (convertToDefaultTimezone)
                datetime = datetime.ToIgbpyDateTime();
            return datetime.ToString("dd/MM/yyyy HH:mm t");
        }

        /// <summary>
        /// Retorna la fecha en formato "dd/MM/yyyy HH:mm t" (o null)
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns>La fecha en formato "dd/MM/yyyy HH:mm t" (o null)</returns>
        public static string ToIgbpyDateTimeString(this DateTime? datetime, bool convertToDefaultTimezone = true)
        {
            return datetime?.ToIgbpyDateTimeString(convertToDefaultTimezone);
        }

        /// <summary>
        /// Convierte una fecha introducida como "dd/MM/yyyy" en un DateTime nullable. Es null en caso de ser una fecha incorrecta.
        /// El Datetime retornado, de no ser null, será el apropiado para la fecha especificada en la franja horaria establecida por
        /// configuración en el Web.Config (de haberla), y no necesariamente para la franja horaria del servidor. Esto quiere decir que
        /// si la franja horaria establecida en la configuración es "Paraguay Standard Time" y el servidor está en España, si se pasa
        /// como fecha "04/11/2016", la fecha que se almacenará será "04/11/2016 04:00 a.m." en la franja horaria de España, que representa
        /// el mismo instante de tiempo que "04/11/2016 12:00 a.m." en la franja horaria de Paraguay.
        /// </summary>
        /// <param name="dateString">Cadena que debe tener el formato "dd/MM/yyyy"</param>
        /// <returns>El datetima correspondiente o null en caso de que sea una cadena erronea.</returns>
        public static DateTime? ToIgbpyDateTime(this string dateString, bool convertToDefaultTomeZone = true)
        {
            DateTime? result = null;
            DateTime dateTime;

            if (DateTime.TryParseExact(dateString, "dd/MM/yyyy", GetDefaultCulture(), DateTimeStyles.AssumeLocal, out dateTime))
            {
                if (convertToDefaultTomeZone)
                {
                    // Si la zona horaria del servidor (local) es diferente a la establecida por defecto con configuración (en el web.config)
                    // Se le aplica la diferencia a la fecha parseada de modo que represente el instante de tiempo apropiado para la última.
                    TimeZoneInfo defaultTimeZone = GetDefaultTimeZoneInfo(),
                        localTimeZone = TimeZoneInfo.Local;

                    if (defaultTimeZone != localTimeZone)
                    {
                        var timeDifference = localTimeZone.GetUtcOffset(dateTime) - defaultTimeZone.GetUtcOffset(dateTime);
                        dateTime = dateTime.AddHours(timeDifference.Hours);
                    }
                }

                // Se retorna la fecha (con las modificaciones apropiadas para la zona horaria)
                result = dateTime;
            }

            return result;
        }
        #endregion

        #region Send notification messages

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="extraData"></param>
        /// <param name="receiverTokens"></param>
        /// <returns></returns>
        public static async Task SendMessage(string message, Globals.CloudMessage.PushMessageType messageType, object extraData, List<CloudMsgToken> receiverTokens, string userId, log4net.ILog logger = null)
        {
            // Se ignora el contexto introducido, porque no se controla su ciclo de vida
            using (IgbpyContext db = new IgbpyContext())
            {

                foreach (var tokensGroup in receiverTokens.GroupBy(x => x.PlatformType))
                {
                    // Dependiendo del tipo de plataforma se procede con un metodo u otro
                    switch (tokensGroup.Key)
                    {
                        case CloudMsgToken.PlatformTypeEnum.Android:
                            if (!await SendGcmMessage(message, messageType, extraData, tokensGroup.Select(x => x.Token).ToList(), db, logger))
                                logger?.Error("API - CloudMsgController - SendMessage - No se pudieron enviar las notificaciones de Google Cloud Messaging");
                            break;
                        case CloudMsgToken.PlatformTypeEnum.iOS:
                            if (!await SendIosMessage(message, messageType, extraData, tokensGroup.Select(x => x.Token).ToList(), db, logger))
                                logger?.Error("API - CloudMsgController - SendMessage - No se pudieron enviar las notificaciones de Apple");
                            break;
                    }
                }

                // Se guarda el registro del mensaje enviado
                try
                {
                    db.SentCloudMsgs.Add(new SentCloudMsg
                    {
                        MessageType = (int)messageType,
                        Message = message,
                        ExtraData = JsonConvert.SerializeObject(extraData),
                        UserId = userId,
                        SentDate = DateTime.Now,
                        UpdateDate = DateTime.Now,
                    });
                    await db.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    logger?.Error("API - CloudMsgController - Error guardando log de mensje enviado", e);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="receiverTokens"></param>
        private static async Task<bool> SendGcmMessage(string message, Globals.CloudMessage.PushMessageType messageType, object extraData, List<string> receiverTokens, IgbpyContext db, log4net.ILog logger = null)
        {
            // Se obtiene el código de servidor para Google Cloud Messages de los settings
            var googleSettings = await db.GetGoogleSettingsAsync();

            // Si no existe, no se puede proseguir
            if (string.IsNullOrEmpty(googleSettings.ServerGcmKey))
            {
                logger?.Error($"API - CloudMesgController - SendGcmMessage. No hay definida una clave de servidor para google");
                return false;
            }

            // Se realiza el envío en segundo plano
            // Se podría tambien directamente crear un hilo, probablemente

            // INFO: esta etiqueta es solo para que Visual Studio no muestre el warning
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Task.Factory.StartNew(() =>
            {
                logger?.Debug($"GCM configuaration - Setting up to send notifications");

                // INFO: Codigo extraído y modificado de: https://github.com/Redth/PushSharp#gcm-sample-usage
                // Configuracion inicial
                // TODO Quitar todos estos logs, ya que sólo van a crecer con el tiempo
                var config = new GcmConfiguration(googleSettings.ServerGcmKey);

                // Se crea un nuevo broker (este es el componente que realmente realizará los envíos)
                var gcmBroker = new GcmServiceBroker(config);

                // Se controla el caso en el que se falle el envío
                gcmBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {
                    aggregateEx.Handle(ex =>
                    {
                        // Comprueba el tipo de excepcion
                        if (ex is GcmNotificationException)
                        {
                            var notificationException = (GcmNotificationException)ex;

                            // Se registra el error de forma acorde
                            var gcmNotification = notificationException.Notification;
                            var description = notificationException.Description;

                            logger?.Debug($"GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                        }
                        else if (ex is GcmMulticastResultException)
                        {
                            var multicastException = (GcmMulticastResultException)ex;

                            foreach (var succeededNotification in multicastException.Succeeded)
                            {
                                logger?.Debug($"GCM Notification Failed: ID={succeededNotification.MessageId}");
                            }

                            foreach (var failedKvp in multicastException.Failed)
                            {
                                var n = failedKvp.Key;
                                var e = failedKvp.Value;

                                logger?.Debug($"GCM Notification Failed: ID={n.MessageId}, Desc={e.Message}");
                            }

                        }
                        else if (ex is DeviceSubscriptionExpiredException)
                        {
                            var expiredException = (DeviceSubscriptionExpiredException)ex;

                            var oldId = expiredException.OldSubscriptionId;
                            var newId = expiredException.NewSubscriptionId;

                            logger?.Debug($"Device RegistrationId Expired: {oldId}");

                            if (!string.IsNullOrWhiteSpace(newId))
                            {
                                // Si este valor no es nulo, la subscripcion ha cambiado y se debería actualizar la base de datos
                                logger?.Debug($"Device RegistrationId Changed To: {newId}");
                            }
                        }
                        else if (ex is RetryAfterException)
                        {
                            var retryException = (RetryAfterException)ex;
                            // Si se tiene un límite, habría que considerar parar de mandar mensajes hasta después de la
                            // fecha descrita por "RetryAfterUtc"
                            logger?.Debug($"GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                        }
                        else
                        {
                            logger?.Debug("GCM Notification Failed for some unknown reason");
                        }

                        // Se marca como controlado
                        return true;
                    });

                    // INFO: podemos trabajar también directamente con la notificación
                };

                gcmBroker.OnNotificationSucceeded += (notification) =>
                {
                    // TODO: Eliminar toda esta cantidad de registros
                    // INFO: Podemos trabajar con la notificación
                    logger?.Debug($"GCM Notification a {notification.To} Sent!");
                };

                // Se inicia el broker
                gcmBroker.Start();
                logger?.Debug($"GCM configuaration - Broker Started");

                // Introducimos la notificación a enviar y solicitamos que sea a todos los elementos
                foreach (var token in receiverTokens)
                {
                    gcmBroker.QueueNotification(new GcmNotification
                    {
                        RegistrationIds = new List<string> { token },
                        Data = JObject.FromObject(new { Message = message, MessageType = (int)messageType, ExtraData = JsonConvert.SerializeObject(extraData) })
                    });
                }

                // Detiene el broker, aunqeu esto hará que espera a que termine de enviar
                // las notificaciones pendientes (se pueden agregar más notificaciones de una)
                gcmBroker.Stop();
                logger?.Debug($"GCM configuaration - Broker Stopped. All notifications should have been sent");
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            // La ejecucion debería estar llevandose a cabo en segundo plano
            // TODO comprobar si los tasks funcionan correctamente así
            return true;
        }


        /// <summary>
        /// Envía un mensaje mediante notificaciones push de iOS a una lista de receptores.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        /// <param name="extraData"></param>
        /// <param name="receiverTokens"></param>
        /// <param name="db"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        private static async Task<bool> SendIosMessage(string message, Globals.CloudMessage.PushMessageType messageType, object extraData, List<string> receiverTokens, IgbpyContext db, log4net.ILog logger = null)
        {
            // Codigo obtenido de https://github.com/Redth/PushSharp


            // Se obtienen las settings de Apple
            var settings = await db.GetAppleSettingsAsync();

            // Se establece el entorno a utilizar desde las settings
            ApnsConfiguration.ApnsServerEnvironment environment = ApnsConfiguration.ApnsServerEnvironment.Sandbox;
            if (settings.GetPushEnvironment() == AppleSettings.PushEnvironments.Production)
                environment = ApnsConfiguration.ApnsServerEnvironment.Production;

            // Configuration (NOTE: .pfx can also be used here)
            var config = new ApnsConfiguration(environment,
                Convert.FromBase64String(settings.PushCertificateFile), settings.PushCertificatePassword);

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Task.Factory.StartNew(() =>
            {

                // Create a new broker
                var apnsBroker = new ApnsServiceBroker(config);

                // Wire up events
                apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {

                    aggregateEx.Handle(ex =>
                    {

                        // See what kind of exception it was to further diagnose
                        if (ex is ApnsNotificationException)
                        {
                            var notificationException = (ApnsNotificationException)ex;

                            // Deal with the failed notification
                            var apnsNotification = notificationException.Notification;
                            var statusCode = notificationException.ErrorStatusCode;

                            logger?.Debug($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}");

                        }
                        else
                        {
                            // Inner exception might hold more useful information like an ApnsConnectionException           
                            logger?.Debug($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                        }

                        // Mark it as handled
                        return true;
                    });
                };

                apnsBroker.OnNotificationSucceeded += (notification) =>
                {
                    logger?.Debug("Apple Notification Sent!");
                };

                // Start the broker
                apnsBroker.Start();

                foreach (var deviceToken in receiverTokens)
                {
                    // Queue a notification to send
                    apnsBroker.QueueNotification(new ApnsNotification
                    {
                        DeviceToken = deviceToken,
                        Payload = JObject.FromObject(new { aps = new { alert = message, sound = "default" }, ExtraData = JsonConvert.SerializeObject(extraData), MessageType = (int)messageType })
                    });
                }

                // Stop the broker, wait for it to finish   
                // This isn't done after every message, but after you're
                // done with the broker
                apnsBroker.Stop();

            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            return true;
        }

        #endregion

        #region Upload Files

        /// <summary>
        /// Guarda el primer fichero recibido en el request (llamada tipo multipart) en la estructura de ficheros del servidor.
        /// Solicita una función para especificar el nombre del fichero a guardar y la ruta virtual en la que se almacenará (formato url "~/Content/algo", creando
        /// los directorios apropiados en el caso de no existir. Es responsabilidad del usuario el usar apropiadamente para evitar errores inconvenientes en el sistema.
        /// </summary>
        /// <param name="nameformat">Funcion por la cual se genera el nombre del nuevo fichero a almacenar, se le pasa como parámetro el nombre
        /// de fichero original enviado en el request. En el caso de que el nombre coincida con el de otro fichero, el anterior será sustituido por el nuevo.</param>
        /// <param name="virtualRoot">Ruta virtual en la que almacenar el nuevo fichero. Si no se especifica, utiliza un directorio por defecto.</param>
        /// <returns>Un objeto que contendrá la ruta virtual (completa) al nuevo fichero almacenado en su propiedad 'Url'. En caso de error, se incluirá en la propiedad
        /// <param name="httpContext">Contexto de la llamada HTTP</param>
        /// <param name="logger">Log en el que registrar posibles errores</param>
        /// <returns>La "url" completa del fichero guardado (formato "~/content/nombrefichero.jpg") o null si ha habido algún error</returns>
        public static string SaveFile(Func<string, string> nameformat, HttpContext httpContext, string virtualRoot = Globals.Urls.UploadsRootUrl)
        {
            string result = null;

            httpContext = httpContext ?? HttpContext.Current;

            if (httpContext != null)
            {
                if (httpContext.Request.Files.Count > 0)
                {
                    // Se obtiene el fichero
                    var file = httpContext.Request.Files[0];

                    // Se preparan los datos para guardar el fichero
                    string root = httpContext.Server.MapPath(virtualRoot),
                        fileName = nameformat(Path.GetFileName(file.FileName)),
                        filePath = Path.Combine(root, fileName);
                    Directory.CreateDirectory(root);

                    // Copia el archivo a disco
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.InputStream.CopyTo(fileStream);
                    }
                    // Una vez hecho esto, se retorna la url de la imagen
                    result = virtualRoot + "/" + fileName;
                }
            }

            return result;
        }

        #endregion

        #region Thumbnails

        /// <summary>
        /// Retorna la url del thumbnail de una imagen dada la url (virtual) de la misma. La thumbnail se genera automaticamente
        /// en caso de no existir.
        /// </summary>
        /// <param name="imageUrl">Url virtual de la imagen de base (p.ej: "~/Content/images/imagen.jpeg")</param>
        /// <param name="httpContext">Contexto de la solicitud HTTP actual</param>
        /// <param name="createIfDoesntExist">Indicador para generar o no el thumbnail si no existe. Por defecto true.</param>
        /// <returns>Una url virtual con la direccion del thumbnail, null en caso de que no exista la imagen inicial.</returns>
        public static string GetThumbnailUrl(string imageUrl, HttpContext httpContext, bool createIfDoesntExist = true)
        {
            string result = null;
            string serverPath = httpContext.Server.MapPath(imageUrl);

            if (File.Exists(serverPath))
            {
                int lastSeparator = imageUrl.LastIndexOf("/");
                string virtualThumbRootPath = imageUrl.Substring(0, lastSeparator) + Globals.Urls.ThumbsSuffix,
                    thumbFileName = imageUrl.Substring(lastSeparator + 1),
                    virtualThumbPath = virtualThumbRootPath + "/" + thumbFileName,
                    serverThumbRootPath = httpContext.Server.MapPath(virtualThumbRootPath),
                    serverThumbPath = httpContext.Server.MapPath(virtualThumbPath);

                bool fileExists = File.Exists(serverThumbPath);

                // Si el fichero de thumbnail no existe, se genera
                if (fileExists || createIfDoesntExist)
                {
                    if (!fileExists)
                    {
                        using (var image = Image.FromFile(serverPath))
                        {
                            // Crea el directorio de thumbnails si no existe
                            Directory.CreateDirectory(serverThumbRootPath);

                            // Se calculan las medidas del thumbnail (de ser necesario)
                            int width = image.Width,
                                height = image.Height;

                            // si la imagen tiene unas dimensiones que superen las de un thumbnail, se debe minimizar
                            if (width > Globals.Api.DefaultMaxThumbSize || height > Globals.Api.DefaultMaxThumbSize)
                            {
                                if (width > height)
                                {
                                    height = (int)Math.Ceiling((((decimal)Globals.Api.DefaultMaxThumbSize) / width) * height);
                                    width = Globals.Api.DefaultMaxThumbSize;
                                }
                                else
                                {
                                    width = (int)Math.Ceiling((((decimal)Globals.Api.DefaultMaxThumbSize) / height) * width);
                                    height = Globals.Api.DefaultMaxThumbSize;
                                }
                            }

                            // Se crea el thumbnail
                            using (var thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero))
                            {
                                thumb.Save(serverThumbPath);
                            }
                        }
                    }
                    // Se retorna la url virtual generada
                    result = virtualThumbPath;
                }
            }

            return result;
        }

        #endregion
    }
}