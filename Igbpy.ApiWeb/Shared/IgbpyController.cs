﻿using Microsoft.AspNet.Identity.Owin;
using Igbpy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using AutoMapper;
using Igbpy.ApiWeb.App_Start;
using Microsoft.AspNet.Identity;

namespace Igbpy.ApiWeb.Shared
{
    public abstract class IgbpyController: Controller
    {
        #region Fields

        public IMapper mapper { get; set; }
        public IMapper mapperDynamic { get; set; }

        public IgbpyController()
        {
            mapper = AutoMapperConfig.mapperConfig.CreateMapper();
            mapperDynamic = AutoMapperConfig.dynamicMapper.CreateMapper();
        }

        private IgbpyContext _db = null;
        protected IgbpyContext Db
        {
            get
            {
                if (_db == null)
                {
                    _db = HttpContext.GetOwinContext().Get<IgbpyContext>();
                }
                return _db;
            }
        }

        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected log4net.ILog Logger
        {
            get
            {
                return _logger;
            }
        }

        protected string UserId
        {
            get
            {
                return User.Identity.GetUserId();
            }

        }
        #endregion




    }
}