﻿using Microsoft.AspNet.Identity.Owin;
using Igbpy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using Igbpy.ApiWeb.Models;
using System.IO;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using System.Web.Http.ModelBinding;
using Igbpy.Domain.IdentityModels;
using Igbpy.ApiWeb.Models.Api;
using AutoMapper;
using Igbpy.ApiWeb.App_Start;

namespace Igbpy.ApiWeb.Shared
{
    public abstract class IgbpyApiController: ApiController
    {
        #region Fields

        public IMapper mapper { get; set; }
        public IMapper mapperDynamic { get; set; }

        public IgbpyApiController()
        {
            mapper = AutoMapperConfig.mapperConfig.CreateMapper();
            mapperDynamic = AutoMapperConfig.dynamicMapper.CreateMapper();
        }

        protected string UserId
        {
            get
            {
                return User.Identity.GetUserId();
            }

        }

        private IgbpyContext _db = null;
        protected IgbpyContext Db
        {
            get
            {
                if (_db == null)
                {
                    _db = Request.GetOwinContext().Get<IgbpyContext>();
                }
                return _db;
            }
        }

        private IgbpyContext.IgbpyUserManager _userManager;
        protected IgbpyContext.IgbpyUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<IgbpyContext.IgbpyUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private SignInManager<User, string> _signInManager;
        protected SignInManager<User, string> SignInManager
        {
            get
            {
                return _signInManager ?? Request.GetOwinContext().Get<SignInManager<User, string>>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected log4net.ILog Logger
        {
            get
            {
                return _logger;
            }
        }
        #endregion

        /// <summary>
        /// Guarda el primer fichero recibido en el request (llamada tipo multipart) en la estructura de ficheros del servidor.
        /// Solicita una función para especificar el nombre del fichero a guardar y la ruta virtual en la que se almacenará, creando
        /// los directorios apropiados en el caso de no existir. Es responsabilidad del usuario el usar apropiadamente para evitar errores inconvenientes en el sistema.
        /// </summary>
        /// <param name="nameformat">Funcion por la cual se genera el nombre del nuevo fichero a almacenar, se le pasa como parámetro el nombre
        /// de fichero original enviado en el request. En el caso de que el nombre coincida con el de otro fichero, el anterior será sustituido por el nuevo.</param>
        /// <param name="virtualRoot">Ruta virtual en la que almacenar el nuevo fichero. Si no se especifica, utiliza un directorio por defecto.</param>
        /// <returns>Un objeto que contendrá la ruta virtual (completa) al nuevo fichero almacenado en su propiedad 'Url'. En caso de error, se incluirá en la propiedad
        /// 'Error' y 'Url' será null.</returns>
        protected FileUploadResponseModel SaveImage(Func<string, string> nameformat, string virtualRoot = "~/Content/images/uploads", HttpContext httpContext = null)
        {
            var result = new FileUploadResponseModel();

            httpContext = httpContext ?? HttpContext.Current;

            if (httpContext != null)
            {

                if (httpContext.Request.Files.Count <= 0)
                    result.Error = Resources.Resources.Common_ErrorMessages_NoUploadedFiles;
                else
                {
                    try
                    {
                        result.Url = Helpers.SaveFile(nameformat, httpContext, virtualRoot);
                    }
                    catch (Exception e)
                    {
                        // Se guarda el error generado
                        Logger.Error("MobielAppTemplateApiController - ERROR", e);
                        result.Error = Resources.Resources.Common_ErrorMessages_ServerError;
                    }
                }
            }
            else
            {
                Logger.Error("API - MobielAppTemplateApiController - HttpContext no accesible");
                result.Error = Resources.Resources.Common_ErrorMessages_ServerError;
            }

            return result;
        }

        /// <summary>
        /// Guarda el primer fichero recibido en el request (llamada tipo multipart) en la estructura de ficheros del servidor.
        /// Solicita una función para especificar el nombre del fichero a guardar y la ruta virtual en la que se almacenará, creando
        /// los directorios apropiados en el caso de no existir. Es responsabilidad del usuario el usar apropiadamente para evitar errores inconvenientes en el sistema.
        /// </summary>
        /// <param name="nameformat">Funcion por la cual se genera el nombre del nuevo fichero a almacenar, se le pasa como parámetro el nombre
        /// de fichero original enviado en el request. En el caso de que el nombre coincida con el de otro fichero, el anterior será sustituido por el nuevo.</param>
        /// <param name="virtualRoot">Ruta virtual en la que almacenar el nuevo fichero. Si no se especifica, utiliza un directorio por defecto.</param>
        /// <returns>Un objeto que contendrá la ruta virtual (completa) al nuevo fichero almacenado en su propiedad 'Url'. En caso de error, se incluirá en la propiedad
        /// 'Error' y 'Url' será null.</returns>
        protected Task<FileUploadResponseModel> SaveImageAsync(Func<string, string> nameformat, string virtualRoot = "~/Content/images/uploads", HttpContext httpContext = null)
        {
            httpContext = httpContext ?? HttpContext.Current;
            return Task.Factory.StartNew(() =>
            {
                return SaveImage(nameformat, virtualRoot, httpContext);
            });
        }


        /// <summary>
        /// Obtiene un elemento para devolver a una solicitud de elementos usando paginado.
        /// </summary>
        /// <typeparam name="TQuery"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="offset">Desplazamiento dentro de la coleccion principal de los elementos a mostrar.</param>
        /// <param name="size">Número de elementos a mostrar de la colaccion.</param>
        /// <param name="getQuery">Funcion que devuelve una query con los elementos filtrados y ordenados entre los que se va a generar la respuesta</param>
        /// <param name="getSelect">Funcion que devuelve una colección de elementos a partir de la query. La query que se recibirá por parámetro ya habrá sido modificada
        /// para devolver la cantidad justa de elementos. Los valores de paginado estarán establecidos correctamente</param>
        /// <returns>Elemento de enlace a transmitir por la red con los datos obtenidos y sus campos de paginación apropiados.</returns>
        protected QueryResponseModel<TResponse> GetQueryResponse<TQuery, TResponse>(
            int offset,
            int size,
            Func<IQueryable<TQuery>> getQuery,
            Func<IQueryable<TQuery>, IEnumerable<TResponse>> getSelect)
        {
            // Query (con filtros y ordenaciones)
            var query = getQuery();

            // Prepara el resultado y sus campos de paginado apropiadamente
            var result = new QueryResponseModel<TResponse>();
            result.SetSafeValues(offset, size, query.Count());

            if (result.Offset > 0)
                query = query.Skip(result.Offset);
            if (result.Size > 0)
                query = query.Take(result.Size);

            // Obtiene los elementos
            result.Items = getSelect(query);

            // Se retorna el resultado
            return result;
        }

        /// <summary>
        /// Obtiene un elemento para devolver a una solicitud de elementos usando paginado (asíncrono).
        /// </summary>
        /// <typeparam name="TQuery"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="offset">Desplazamiento dentro de la coleccion principal de los elementos a mostrar.</param>
        /// <param name="size">Número de elementos a mostrar de la colaccion.</param>
        /// <param name="getQuery">Funcion que devuelve una query con los elementos filtrados y ordenados entre los que se va a generar la respuesta</param>
        /// <param name="getSelect">Funcion que devuelve una colección de elementos a partir de la query. La query que se recibirá por parámetro ya habrá sido modificada
        /// para devolver la cantidad justa de elementos. Los valores de paginado estarán establecidos correctamente</param>
        /// <returns>Elemento de enlace a transmitir por la red con los datos obtenidos y sus campos de paginación apropiados.</returns>
        protected Task<QueryResponseModel<TResponse>> GetQueryResponseAsync<TQuery, TResponse>(
            int offset,
            int size,
            Func<IQueryable<TQuery>> getQuery,
            Func<IQueryable<TQuery>, IEnumerable<TResponse>> getSelect)
        {
            return Task.Factory.StartNew(() => GetQueryResponse(offset, size, getQuery, getSelect));
        }

        /// <summary>
        /// Retorna una respuesta con un error.
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected NegotiatedContentResult<ResponseModel> Error(System.Net.HttpStatusCode statusCode, string message)
        {
            return Content(statusCode, new ResponseModel { Error = message });
        }

        /// <summary>
        /// Retorna una respuesta con un error, obteniendo el mensaje de un ModelState.
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        protected NegotiatedContentResult<ResponseModel> Error(System.Net.HttpStatusCode statusCode, ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
                return Error(statusCode, modelState.GetFirstError());
            else
                return Error(statusCode, Resources.Resources.Common_ErrorMessages_ValidationError);
        }

        /// <summary>
        /// Prepara el resultado de error para devolver a partir de un resultado de un IdentityResult
        /// </summary>
        /// <param name="identityResult"></param>
        /// <returns></returns>
        protected IHttpActionResult GetErrorResult(IdentityResult identityResult)
        {
            if (identityResult != null)
            {
                if (!identityResult.Succeeded)
                {
                    // Se devuelve el primero de los errores encontrados
                    if (identityResult.Errors != null)
                    {
                        return Error(System.Net.HttpStatusCode.BadRequest, identityResult.Errors.FirstOrDefault());
                    }
                    // Se devuelve simplemente un mensaje de error en el request
                    return Error(System.Net.HttpStatusCode.BadRequest, Resources.Resources.Common_ErrorMessages_BadRequest);

                }
            }

            // Si se llega hasta aqui, algo ha debido ir mal igual, así que se envñia un mensaje de Error en el servidor
            return Error(System.Net.HttpStatusCode.InternalServerError, Resources.Resources.Common_ErrorMessages_ServerError);

        }
    }
}