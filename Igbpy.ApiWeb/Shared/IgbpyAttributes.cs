﻿using Igbpy.ApiWeb.Models.Api;
using Igbpy.Domain.Extensions.Users;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Routing;

namespace Igbpy.ApiWeb.Shared
{
    public class IgbpyApiAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Si se detecta un acceso no autorizado, se retorna un objeto de respuesta del tipo { Error: '....' }
        /// </summary>
        /// <param name="actionContext"></param>
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);
            actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized)
            {
                Content = new StringContent(JsonConvert.SerializeObject(new ResponseModel { Error = Resources.Resources.Common_ErrorMessages_NotLoggedIn }))
            };
        }


        public class IgbpyAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
        {
            // Custom property
            public string Controller { get; set; }
            public string Action { get; set; }


            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                var isAuthorized = base.AuthorizeCore(httpContext);
                if (!isAuthorized)
                {
                    return false;
                }

                if (!string.IsNullOrEmpty(Controller) || !string.IsNullOrEmpty(Action))
                    return Extensions.HasPermission(httpContext.User.Identity.GetUserId(), Controller, Action);

                return true;
            }

            protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
            {
                if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    base.HandleUnauthorizedRequest(filterContext);
                }
                else
                {
                    filterContext.Result = new System.Web.Mvc.RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Home", action = "NotAllowed" }));
                }
            }
        }

    }
}