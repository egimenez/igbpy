﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Shared
{
    public static class Globals
    {
        public static class AppSettings
        {
            public const string GoogleClientId = "google.clientId";
            public const string GoogleClientSecret = "google.clientSecret";
            public const string DefaultCulture = "default.culture";
            public const string DefaultTimeZone = "default.timeZone";
        }

        public static class Urls
        {
            public const string ThumbsSuffix = "/thumbs";
            public const string UploadsRootUrl = "~/Content/uploads";
            public const string NoImageVirtualUrl = "~/Content/images/no_image.svg";
        }

        public static class ExternalUrls
        {
            public const string Gmaps = "http://www.google.com/maps/place/";
            public const string Youtube = "https://www.youtube.com/watch?v=";
        }

        public static class Api
        {
            public const int DefaultSize = 10;
            public const int DefaultMaxThumbSize = 250;
        }

        public static class MapDefaults
        {
            public const string Latitude = "-23.514410626104983";
            public const string Longitude = "-58.204137681396446";
            public const int Zoom = 8;
        }

        public static class CloudMessage
        {
            public enum PushMessageType
            {
                Message = 1,
                Campaign = 2,
            }

            public enum PushSubMessageType
            {
                Alert = 1,
                Truck = 2,
            }
        }
    }
}