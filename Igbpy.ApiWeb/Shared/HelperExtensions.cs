﻿using Igbpy.Domain;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Igbpy.Domain.Extensions.Settings;
using System.Reflection;

namespace Igbpy.ApiWeb.Shared
{
    public static class HelperExtensions
    {
        /// <summary>
        /// Agrega los scripts apropiados de Dropzone
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString AddDropzoneScripts(this HtmlHelper html, bool autoDiscover = false)
        {
            return MvcHtmlString.Create(
                $"<link href=\"{UrlHelper.GenerateContentUrl("~/Content/dropzone/basic.css", html.ViewContext.HttpContext)}\" rel=\"stylesheet\" />\n" +
                $"<link href=\"{UrlHelper.GenerateContentUrl("~/Content/dropzone/dropzone.css", html.ViewContext.HttpContext)}\" rel=\"stylesheet\" />\n" +
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/dropzone.js", html.ViewContext.HttpContext)}\"></script>" +
                (!autoDiscover?$"\n<script>Dropzone.autoDiscover = false;</script>" :string.Empty));
        }


        /// <summary>
        /// Agrega los scripts apropiados de jQueryUi
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString AddJQueryUiScripts(this HtmlHelper html)
        {
            return MvcHtmlString.Create(
                $"<link href=\"{UrlHelper.GenerateContentUrl("~/Content/jquery-ui-1.11.4.custom/jquery-ui.min.css", html.ViewContext.HttpContext)}\" rel=\"stylesheet\" />\n" +
                $"<link href=\"{UrlHelper.GenerateContentUrl("~/Content/jquery-ui-1.11.4.custom/jquery-ui.structure.min.css", html.ViewContext.HttpContext)}\" rel=\"stylesheet\" />\n" +
                $"<link href=\"{UrlHelper.GenerateContentUrl("~/Content/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css", html.ViewContext.HttpContext)}\" rel=\"stylesheet\" />\n" +
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/jquery-ui-1.11.4.custom/jquery-ui.min.js", html.ViewContext.HttpContext)}\"></script>");
        }

        /// <summary>
        /// Agrega los scripts apropiados de React
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString AddReactScripts(this HtmlHelper html)
        {
            return MvcHtmlString.Create(
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/react.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/react-dom.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/eventbus.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/SiteReactComponents.js", html.ViewContext.HttpContext)}\"></script>");
        }

        /// <summary>
        /// Agrega los scripts apropiados para el datePicker (se requieren los scripts de jQuery UI)
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString AddDatePickerScripts(this HtmlHelper html)
        {
            return MvcHtmlString.Create(
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/jquery-ui-1.11.4.custom/datepicker-es.js", html.ViewContext.HttpContext)}\"></script>\n" +
                "<script>jQuery.datepicker.setDefaults(jQuery.datepicker.regional[\"es\"]);</script>");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="callbackFunctionName"></param>
        /// <returns></returns>
        public static MvcHtmlString AddGoogleMapsScript(this HtmlHelper html, string callbackFunctionName = null)
        {
            var googleSettings = html.ViewContext.HttpContext.GetOwinContext().Get<IgbpyContext>().GetGoogleSettingsAsync().GetAwaiter().GetResult();
            string googleMapsApiUrl = "https://maps.googleapis.com/maps/api/js?libraries=places";
            if (!string.IsNullOrWhiteSpace(googleSettings.BrowserApiKey))
                googleMapsApiUrl += "&key=" + googleSettings.BrowserApiKey;
            if (!string.IsNullOrWhiteSpace(callbackFunctionName))
                googleMapsApiUrl += "&callback="+callbackFunctionName;

            return MvcHtmlString.Create(string.Format("<script {1} type=\"text/javascript\" src=\"{0}\"></script>", googleMapsApiUrl, string.IsNullOrEmpty(callbackFunctionName) ? string.Empty : "refer async"));
        }


        /// <summary>
        ///     Método genérico de extensión que ayuda a reflejar y recuperar cualquier atributo que se aplique a un 'Enum'.
        /// </summary>
        /// <returns></returns>
        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
                where TAttribute : Attribute
        {
            var type = enumValue.GetType();
            var field = type.GetField(enumValue.ToString());
            var attr = field.GetCustomAttribute(typeof(TAttribute)) as TAttribute;
            return attr;
        }

        /// <summary>
        /// Agrega los scripts apropiados de Boostrap para el datetimepicker
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString AddDateTimePickerScripts(this HtmlHelper html)
        {
            return MvcHtmlString.Create(
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/moment.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
                $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/moment-with-locales.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
            $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/bootstrap/collapse.js", html.ViewContext.HttpContext)}\"></script>\n" +
            $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/bootstrap/bootstrap-datetimepicker.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
            $"<script src=\"{UrlHelper.GenerateContentUrl("~/Scripts/locales.min.js", html.ViewContext.HttpContext)}\"></script>\n" +
            $"<link href=\"{UrlHelper.GenerateContentUrl("~/Content/bootstrap/css/bootstrap-datetimepicker.min.css", html.ViewContext.HttpContext)}\" rel=\"stylesheet\" />\n");
        }

        /// <summary>
        ///     Devuelve la representación en texto del número pasado como parámetro.
        /// </summary>
        /// <param name="num">Se recibe como cadena, para facilitar el manejo.</param>
        /// <returns></returns>
        public static string NumberInChars(string num, bool hasDecimals)
        {
            string res, dec = "";
            int decimals;
            long integer;
            double number;

            try
            {
                number = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            if (hasDecimals)
                integer = Convert.ToInt64(Math.Round(number, 0));
            else
                integer = Convert.ToInt64(Math.Truncate(number));

            decimals = Convert.ToInt32(Math.Round((number - integer) * 100, 2));

            if (decimals > 0 && !hasDecimals)
                dec = " CON " + decimals.ToString() + "/100.--";
            else if (!hasDecimals)
                dec = " CON 00/100.--";

            res = toText(Convert.ToDouble(integer), false) + dec;

            return res;
        }


        public static string toText(double value, bool isUN)
        {
            string num2Text = "";

            value = Math.Truncate(value);

            if (value == 0) num2Text = "CERO";

            else if (value == 1 && !isUN) num2Text = "UNO";

            else if (value == 1 && isUN) num2Text = "UN";

            else if (value == 2) num2Text = "DOS";

            else if (value == 3) num2Text = "TRES";

            else if (value == 4) num2Text = "CUATRO";

            else if (value == 5) num2Text = "CINCO";

            else if (value == 6) num2Text = "SEIS";

            else if (value == 7) num2Text = "SIETE";

            else if (value == 8) num2Text = "OCHO";

            else if (value == 9) num2Text = "NUEVE";

            else if (value == 10) num2Text = "DIEZ";

            else if (value == 11) num2Text = "ONCE";

            else if (value == 12) num2Text = "DOCE";

            else if (value == 13) num2Text = "TRECE";

            else if (value == 14) num2Text = "CATORCE";

            else if (value == 15) num2Text = "QUINCE";

            else if (value < 20) num2Text = "DIECI" + toText(value - 10, false);

            else if (value == 20) num2Text = "VEINTE";

            else if (value < 30) num2Text = "VEINTI" + toText(value - 20, isUN);

            else if (value == 30) num2Text = "TREINTA";

            else if (value == 40) num2Text = "CUARENTA";

            else if (value == 50) num2Text = "CINCUENTA";

            else if (value == 60) num2Text = "SESENTA";

            else if (value == 70) num2Text = "SETENTA";

            else if (value == 80) num2Text = "OCHENTA";

            else if (value == 90) num2Text = "NOVENTA";

            else if (value < 100) num2Text = toText(Math.Truncate(value / 10) * 10, false) + " Y " + toText(value % 10, isUN);

            else if (value == 100) num2Text = "CIEN";

            else if (value < 200) num2Text = "CIENTO " + toText(value - 100, isUN);

            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) num2Text = toText(Math.Truncate(value / 100), isUN) + "CIENTOS";

            else if (value == 500) num2Text = "QUINIENTOS";

            else if (value == 700) num2Text = "SETECIENTOS";

            else if (value == 900) num2Text = "NOVECIENTOS";

            else if (value < 1000) num2Text = toText(Math.Truncate(value / 100) * 100, false) + " " + toText(value % 100, isUN);

            else if (value == 1000) num2Text = "MIL";

            else if (value < 2000) num2Text = "MIL " + toText(value % 1000, false);

            else if (value < 1000000)
            {
                num2Text = toText(Math.Truncate(value / 1000), true) + " MIL";

                if ((value % 1000) > 0) num2Text = num2Text + " " + toText(value % 1000, isUN);
            }

            else if (value == 1000000) num2Text = "UN MILLON";

            else if (value < 2000000) num2Text = "UN MILLON " + toText(value % 1000000, false);


            else if (value < 1000000000000)
            {
                num2Text = toText(Math.Truncate(value / 1000000), true) + " MILLONES ";

                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) num2Text = num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000, isUN);
            }

            else if (value == 1000000000000) num2Text = "UN BILLON";

            else if (value < 2000000000000) num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000, false);

            else
            {
                num2Text = toText(Math.Truncate(value / 1000000000000), true) + " BILLONES";

                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) num2Text = num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000, isUN);
            }

            return num2Text;

        }
    }
}