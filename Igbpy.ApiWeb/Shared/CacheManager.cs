﻿using System;
using System.Web;
using System.Collections;
using System.Runtime.Caching;
using System.Linq;

namespace Igbpy.ApiWeb.Shared
{
    public class CacheManager
    {
        ObjectCache cache = MemoryCache.Default;



        #region CacheManager Miembros



        public void InsertCache(string Key, object objeto, int minutos = 120)
        {

            if (objeto == null)
                return;
            //HttpRuntime.Cache.Insert(Key, objeto, null, DateTime.Now.AddMinutes(minutos), System.Web.Caching.Cache.NoSlidingExpiration);

            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(minutos);

            cache.Add(Key, objeto, policy);
        }



        public object GetDatosCache(string Key)
        {
            //return HttpRuntime.Cache.Get(Key);
            return cache.Get(Key);
        }


        public bool Contains(string key)
        {
            //return HttpRuntime.Cache.Get(key) != null;
            return cache.Get(key) != null;
        }

        public int Count()
        {
            //return HttpRuntime.Cache.Count;
            return (int)cache.GetCount();
        }

        public void Remove(string key)
        {
            //HttpRuntime.Cache.Remove(key);
            cache.Remove(key);
        }



        public void RemoveAllCache()
        {

            foreach (var item in cache.ToList())
            {
                cache.Remove(item.Key);

            }


        }

        #endregion ICacheManager Miembros
    }
}