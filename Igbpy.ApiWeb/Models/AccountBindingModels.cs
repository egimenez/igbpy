﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

/// <summary>
/// INFO: Las clases definidas en este fichero se han heredado del template por defecto de WebApi de Visual Studio.
/// Usar y modificar con precaución.
/// </summary>
namespace Igbpy.ApiWeb.Models
{
    // Modelos usados como parametros para acciones de la cuenta (API y WEB)
    public class LoginBindingModel
    {
        
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public List<ExternalLoginViewModel> ExternalLogins { get; set; }

        public LoginBindingModel()
        {
            ExternalLogins = new List<ExternalLoginViewModel>();
        }
    }

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "Token de acceso externo")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        public string OldPassword { get; set; }

        [Required]
        [MinLength(6)]
        public string NewPassword { get; set; }

        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public String Name { get; set; }

        public String Imei { get; set; }

        public String Platform { get; set; }

        public String PhoneNumber { get; set; }

    }

    public class RegisterExternalBindingModel
    {
        [Required(ErrorMessage = "El {0} es requerido"), EmailAddress(ErrorMessage = "El {0} debe ser un email válido")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Proveedor de acceso")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Clave del proveedor")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [MinLength(6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nuevo password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme el nuevo password")]
        [Compare("NewPassword", ErrorMessage = "El nuevo password y su confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }
    }
}
