﻿using Igbpy.ApiWeb.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Conjunto de clases base a utilizar para llamadas y respuestas para y desde la API.
/// </summary>
namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Argumentos de entrada a enviar a una solicitud de búsqueda de datos
    /// </summary>
    public class QueryRequestModel
    {
        /// <summary>
        /// Desplazamiento del primer resultado dentro del conjunto total de los mismos (raiz 0)
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Cantidad de elementos a retornar como máximo
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Incluir elementos "eliminados" de forma lógica dentro de los resultados a obtener
        /// </summary>
        public bool IncludeDeleted { get; set; }
    }
    

    /// <summary>
    /// Modelo de respuesta base para cualquier llamada a un método de la API
    /// </summary>
    public class ResponseModel
    {
        /// <summary>
        /// Mensaje de error, sólo está cargado en caso de haber alguno. En otro caso
        /// se puede considerar normalmente que la respuesta es correcta.
        /// </summary>
        public string Error { get; set; }
    }

    /// <summary>
    /// Modelo de respuesta base para llamadas a métodos de la API con objetivo de crear un nuevo elemento.
    /// </summary>
    /// <typeparam name="T">Tipo de dato del identificador del elemento a crear</typeparam>
    public class CreateResponseModel<T> : ResponseModel
    {
        /// <summary>
        /// El nuevo identificador del elemento creado.
        /// </summary>
        public T Id { get; set; }
    }

    /// <summary>
    /// Modelo de respuesta base para llamadas a métodos de la API con objetivo de subir un fichero.
    /// </summary>
    public class FileUploadResponseModel : ResponseModel
    {
        /// <summary>
        /// Url de acceso externo al fichero
        /// </summary>
        public string Url { get; set; }
    }

    /// <summary>
    /// Modelo de respuesta base para llamadas a métodos de la API con objetivo de obtener múltiples elementos.
    /// </summary>
    /// <typeparam name="T">Tipo de dato de los elementos a retornar</typeparam>
    public class QueryResponseModel<T> : ResponseModel
    {
        /// <summary>
        /// Colección de elementos a retornar
        /// </summary>
        public IEnumerable<T> Items { get; set; }

        /// <summary>
        /// Desplazamiento del primer elemento retornado dentro del total de elementos de ese tipo en el sistema
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Cantidad de elementos retornados
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Cantidad total de elementos existentes en el sistema para la consulta realizada
        /// </summary>
        public int TotalSize { get; set; }

        /// <summary>
        /// Establece apropiadamente los campos de Offset, Size y TotalSize del objeto
        /// a partir de unos valores de solicitud. Tras ejecutar este método, se puede asegurar lo siguiente de esos campos:
        ///  - TotalSize tendrá un valor mayor o igual que cero
        ///  - Size tendrá un valor mayor a cero, o bien el tamaño por defecto por la aplicacion
        ///  - Offset tendrá un valor mayor o igual que cero que permitirá siempre obtener alguna cantidad de datos
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="size"></param>
        /// <param name="totalSize"></param>
        public void SetSafeValues(int offset, int size, int totalSize)
        {
            TotalSize = Math.Max(0, totalSize);
            Size = size > 0 ? size : Globals.Api.DefaultSize;
            Offset = Math.Max(0, offset);

            if (TotalSize > 0)
            {
                if (Offset > TotalSize-1)
                {
                    var overflow = TotalSize % Size;
                    Offset = TotalSize - (overflow > 0 ? overflow : Size);
                }
            }
        }

/// <summary>
/// Constructor vacío
/// </summary>
public QueryResponseModel()
        {
            Items = new T[] { };
        }
    }
}