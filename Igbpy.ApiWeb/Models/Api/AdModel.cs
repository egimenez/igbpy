﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Clase de modelo de transporte con los datos de un anuncio
    /// </summary>
    public class AdModel
    {
        public int Id { get; set; }

        public string ImageUrl { get; set; }
        public string ImageThumbUrl { get; set; }

        [Required, Range(1, Int32.MaxValue)]
        public int Order { get; set; }

        [Required, Range(1, Int32.MaxValue)]
        public int Seconds { get; set; }

        public bool Deleted { get; set; }
    }
}