﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Modelo de transporte para tokens de envío de notificaciones a celulares
    /// </summary>
    public class CloudMsgTokenModel
    {
        public int Id { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        public int Platform { get; set; }

        public string UpdateDate { get; set; }
    }
}