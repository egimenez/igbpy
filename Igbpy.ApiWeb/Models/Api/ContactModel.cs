﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Modelo de transporte para mensajes de contacto con la web
    /// </summary>
    public class ContactModel
    {
        public int Id { get; set; }

        [Required]
        public string Message { get; set; }

        public string CreateDate { get; set; }

        public UserModel User { get; set; }

        public class UserModel
        {
            [Required]
            public string Id { get; set; }
            public string UserName { get; set; }
        }
    }
}