﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Clase de modelo de transporte con los datos de un anuncio
    /// </summary>
    public class GenericModel
    {
        public int Id { get; set; }

        public string String { get; set; }

        public string Location { get; set; }

        public string Date { get; set; }

        public string DateTime { get; set; }

        public bool Boolean { get; set; }

        public int Integer { get; set; }

        public bool Deleted { get; set; }
    }
}