﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Modelo para solicitudes a la API de elementos de mensajes enviados a celulares (con sus propios filtros)
    /// </summary>
    public class SentCloudMsgQueryRequestModel : QueryRequestModel
    {
        public int MessageType { get; set; }
    }
}