﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Modelo de transporte para campañas
    /// </summary>
    public class CampaignModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string ImageThumbUrl { get; set; }

        [RegularExpression(Domain.Shared.Globals.Regex.UrlOrEmpty)]
        public string WebUrl { get; set; }

        public bool Deleted { get; set; }
    }
}