﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Modelo de transporte para un mensaje a enviar mediante notificaciones de celular
    /// </summary>
    public class CloudMsgModel
    {
        [Required]
        public string Message { get; set; }

        public IEnumerable<int> ReceivingTokensIds { get; set; }

        public CloudMsgModel()
        {
            ReceivingTokensIds = new List<int>();
        }
    }
}