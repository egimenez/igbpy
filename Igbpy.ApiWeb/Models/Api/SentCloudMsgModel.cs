﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Api
{
    /// <summary>
    /// Modelo de transporte para notificaciones por celular enviadas
    /// </summary>
    public class SentCloudMsgModel
    {
        public int Id { get; set; }
        public int MessageType { get; set; }
        public string Message { get; set; }
        public JObject ExtraData { get; set; }
        public UserModel User { get; set; }
        public string SentDate { get; set; }

        public class UserModel
        {
            public string Id { get; set; }
            public string UserName { get; set; }
        }
    }
}