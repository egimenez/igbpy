﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Manage
{
    public class ChangePasswordModel
    {
        public bool HasPassword { get; set; }

        [Display(Name = "Manage_ChangePassword_Fields_CurrentPassword", ResourceType = typeof(Resources.Resources))]
        public string CurrentPassword { get; set; }

        [Display(Name = "Manage_ChangePassword_Fields_NewPassword", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [MinLength(6, ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_MinLength")]
        public string NewPassword { get; set; }

        [Display(Name = "Manage_ChangePassword_Fields_ConfirmNewPassword", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Compare")]
        public string ConfirmNewPassword { get; set; }

    }
}