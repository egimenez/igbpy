﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Account
{
    /// <summary>
    /// Modelo con los datos de la vista para la acción "ForgotPassword"
    /// </summary>
    public class ForgotPasswordViewModel
    {
        [Display(Name = "Account_ForgotPassword_Fields_Email", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Email")]
        public string Email { get; set; }
    }
}