﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Account
{
    /// <summary>
    /// Modelo con los datos necesarios para la vista de "ResetPassword"
    /// </summary>
    public class ResetPasswordViewModel
    {
        [Display(Name = "Account_ResetPassword_Fields_Email", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Email")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        public string Code { get; set; }

        [Display(Name = "Account_ResetPassword_Fields_Password", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [MinLength(6, ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_MinLength")]
        public string Password { get; set; }

        [Display(Name = "Account_ResetPassword_Fields_ConfirmPassword", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Compare")]
        public string ConfirmPassword { get; set; }
    }
}