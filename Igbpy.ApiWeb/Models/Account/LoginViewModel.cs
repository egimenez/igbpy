﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Account
{
    public class LoginViewModel
    {
        [Display(Name = "Account_Login_Fields_Email", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Email")]
        public string Email { get; set; }

        [Display(Name = "Account_Login_Fields_Password", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        public string Password { get; set; }

        public List<ExternalLoginViewModel> ExternalLogins { get; set; }

        public LoginViewModel()
        {
            ExternalLogins = new List<ExternalLoginViewModel>();
        }
    }
}