﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Igbpy.ApiWeb.Models.Account
{
    /// <summary>
    /// Modelo con los datos necesarios para la vista de la acción "Register"
    /// </summary>
    public class RegisterViewModel
    {

        [Display(Name = "Account_Register_Fields_Email", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Email")]
        public string Email { get; set; }


        [Display(Name = "Account_Register_Fields_Password", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [MinLength(6, ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_MinLength")]
        public string Password { get; set; }

        [Display(Name = "Account_Register_Fields_ConfirmPassword", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Required")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "Common_ValidationError_Compare")]
        public string ConfirmPassword { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

    }
}