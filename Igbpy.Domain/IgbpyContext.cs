﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Igbpy.Domain.IdentityModels;
using Igbpy.Domain.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity;
using Resources;
using Igbpy.Domain.Shared;

namespace Igbpy.Domain
{
    public class IgbpyContext : IdentityDbContext<User>
    {
        private readonly static string _sPrivateLock = "Igbpy.Domain.IgbpyContext._sPrivateLock";

        public DbSet<Ads> Ads { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<QueuedEmail> QueuedEmails { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Generic> Generics { get; set; }
        public DbSet<PrintCoordinates> PrintCoordinates { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PermissionRole> PermissionRoles { get; set; }


        public DbSet<CloudMsgToken> CloudMsgToken { get; set; }

        public DbSet<SentCloudMsg> SentCloudMsgs { get; set; }

        public DbSet<Campaign> Campaigns { get; set; }

        public DbSet<Language> Languages { get; set; }
        public DbSet<LanguageItem> LanguageItems { get; set; }

        // INFO: La base de datos se inicializa y/o actualiza en su última versión
        static IgbpyContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<IgbpyContext, Domain.Migrations.Configuration>());
        }

        public IgbpyContext()
                : base("IgbpyConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Descomentar para que no se creen por defecto las convenciones 
            // de las operaciones 'onCascade delete' establecidas por defecto 
            //// INFO: Con esto evitamos que se generen las restricciones de "ON CASCADE DELETE" iniciales
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToOneConstraintIntroductionConvention>();

            // Se modifican los nombres de las tablas por defecto creadas por Identity
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UsersLogins");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UsersRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UsersClaims");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
        }

        /// <summary>
        /// Clase que controla la creacion de nuevos elementos
        /// </summary>
        public class IgbpyUserManager : UserManager<User>
        {
            public IgbpyUserManager(IUserStore<User> store) : base(store)
            {
                // Configurar lógica de validacion de usuarios
                this.UserValidator = new IgbpyUserValidator();

                // Configurar lógica de validación de passwords
                this.PasswordValidator = new IgbpyPasswordValidator();
            }
        }

        /// <summary>
        /// Validador personalizado para los usuarios
        /// </summary>
        public class IgbpyUserValidator : IIdentityValidator<User>
        {
            public Task<IdentityResult> ValidateAsync(User item)
            {
                // INFO: Llevar a cabo la validación del usuario
                // Que no exista otro usuario con el mismo email, comprobar que sólo pueda tener una serie de caracteres su nombre,
                // que no se repita el teléfono, etc
                return Task.Factory.StartNew(() =>
                {
                    var errors = new List<string>();

                    #region Comprobaciones de los campos propios de la entidad

                    // INFO: Implementar si corresponde 

                    #endregion

                    #region Comprobaciones con respecto a usuarios ya existentes (username o emails repetidos)

                    if (errors.Count == 0)
                    {
                        // INFO: El siguiente código se tiene que ejecutar en una región crítica para evitar condiciones de carrera
                        lock (_sPrivateLock)
                        {
                            using (IgbpyContext db = new IgbpyContext())
                            {
                                // INFO: Incluir más condinciones si corresponde

                                var existingUser = db.Users.Where(x => x.Id != item.Id && (x.UserName.ToLower() == item.UserName.ToLower() || x.Email.ToLower() == item.Email.ToLower())).ToArray();

                                if (existingUser.Length > 0)
                                {
                                    if (existingUser.Any(x => x.UserName.ToLower() == item.UserName.ToLower()))
                                        errors.Add(Resources.Resources.UserValidation_ExistingUser);
                                    if (existingUser.Any(x => x.Email.ToLower() == item.Email.ToLower()))
                                        errors.Add(Resources.Resources.UserValidation_ExistingEmail);
                                }

                            }
                        }
                    }

                    #endregion

                    if (errors.Count > 0)
                        return new IdentityResult(errors);
                    return IdentityResult.Success;
                });
            }
        }

        /// <summary>
        /// Validador personalizado para las contraseñas
        /// </summary>
        public class IgbpyPasswordValidator : IIdentityValidator<string>
        {
            public Task<IdentityResult> ValidateAsync(string item)
            {
                return Task.Factory.StartNew(() =>
                {
                    var errors = new List<string>();

                    // INFO: Modificar lógica como sea necesario
                    if (string.IsNullOrWhiteSpace(item))
                        errors.Add(Resources.Resources.UserValidation_PasswordRequired);
                    else
                    {
                        // - Tamaño mínimo del password
                        item = item.Trim();
                        if (item.Length < Globals.DatabaseDefaults.UserValidation.PasswordMinLength)
                            errors.Add(string.Format(Resources.Resources.UserValidation_PasswordMinLength, Globals.DatabaseDefaults.UserValidation.PasswordMinLength));
                    }

                    if (errors.Count > 0)
                        return new IdentityResult(errors);
                    return IdentityResult.Success;
                });
            }
        }

    }
}