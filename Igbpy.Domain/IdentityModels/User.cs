﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.IdentityModels
{
    public class User: IdentityUser
    {
        public User():base()
        {
        }

        public User(string userName): base(userName)
        {
            
        }


        // Agregar propiedades adicionales personalizadas para añadir a las creadas por Identity
        public String Name { get; set; }

        public String Imei { get; set; }

        [Required]
        public String Platform { get; set; }

      


    }
}
