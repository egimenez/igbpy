namespace Igbpy.Domain.Migrations
{
    using IdentityModels;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using Shared;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Globalization;
    using System.Linq;
    using System.Resources;
    public sealed class Configuration : DbMigrationsConfiguration<Igbpy.Domain.IgbpyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Igbpy.Domain.IgbpyContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            // Roles (se crean los que falten)
            var currentRoles = context.Roles.Select(x => x.Name).ToArray();
            foreach (string roleName in Globals.Roles.AllRoles.Where(x => !currentRoles.Contains(x)))
            {
                context.Roles.Add(new IdentityRole(roleName));
            }
            context.SaveChanges();

            // Usuario Administrador Inicial
            if (context.Roles.Where(x => x.Name == Globals.Roles.Admin && x.Users.Any()).SingleOrDefault() == null)
            {
                var adminUser = new User(Globals.DatabaseDefaults.AdminUser.UserName)
                {
                    Email = Globals.DatabaseDefaults.AdminUser.Email,
                    Platform = Globals.Platforms.Web
                };

                var userManager = new UserManager<User>(new UserStore<User>(context));
                var result = userManager.Create(adminUser, Globals.DatabaseDefaults.AdminUser.Password);
                if (!result.Succeeded)
                    throw new Exception("Error al crear el usuario administrador inicial.\n" + string.Join("\n", result.Errors.Select(x => "- " + x)));

                result = userManager.AddToRole(adminUser.Id, Globals.Roles.Admin);
                if (!result.Succeeded)
                    throw new Exception("Error al agregar el rol al usuario administrador inicial.\n" + string.Join("\n", result.Errors.Select(x => "- " + x)));
            }

            // Usuario Invitado Inicial
            // INFO: Necesario para el caso de que usuarios no identificados en el sistema puedan realizar operaciones que creen/modifiquen elementos de 
            // la base de datos que hereden del modelo BaseAuditedModel
            // IMPORTANTE: No modificar este usuario desde la aplicaci�n ni directamente desde la base de datos
            if (!context.Users.Any(x => x.UserName == Globals.DatabaseDefaults.GuestUser.UserName))
            {
                var guestUser = new User(Globals.DatabaseDefaults.GuestUser.UserName)
                {
                    Email = Globals.DatabaseDefaults.GuestUser.Email,
                    Platform = Globals.Platforms.Web
                };

                var userManager = new UserManager<User>(new UserStore<User>(context));
                var result = userManager.Create(guestUser);  // INFO: Se crea sin password para que no se pueda hacer login con �l
                if (!result.Succeeded)
                    throw new Exception("Error al crear el usuario invitado inicial.\n" + string.Join("\n", result.Errors.Select(x => "- " + x)));
            }


            //Seccion de idiomas. Ahora obtenemos del resources ya creado

            var UserID = context.Roles.Where(x => x.Name == Globals.Roles.Admin && x.Users.Any()).SingleOrDefault().Users.First().UserId;

            var language = context.Languages.FirstOrDefault(x => x.Code == CultureInfo.CurrentUICulture.Name);

            if (language == null)
            {
                language = new Language
                {
                    Name = "Espa�ol",
                    Code = "es-ES",
                    CreateDate = DateTime.Now,
                    UserId = UserID,
                    UpdateDate = DateTime.Now,
                    Deleted = false,
                };

                context.Languages.Add(language);
            }


            var items = language.LanguageItems.ToList();

            ResourceSet resourceSet = Resources.Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                string resourceKey = entry.Key.ToString();
                object resource = entry.Value;

                if (!items.Any(x => x.Key == resourceKey))
                    language.LanguageItems.Add(new LanguageItem { Key = resourceKey, Value = entry.Value.ToString() });
            }

            context.SaveChanges();


            #region permisos&roles


            var RoleID = context.Roles.Where(x => x.Name == Globals.Roles.Admin && x.Users.Any()).SingleOrDefault().Id;

            var permisosDb = context.Permissions.ToList();
            var permisos = new List<Permission>();


            //Agregamos los permisos

            //Generic
            permisos.Add(new Permission { Action = "Visualizar", Controller = "Generico", CreateUserId = UserID, UpdateUserId = UserID, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
            permisos.Add(new Permission { Action = "Crear", Controller = "Generico", CreateUserId = UserID, UpdateUserId = UserID, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
            permisos.Add(new Permission { Action = "Editar", Controller = "Generico", CreateUserId = UserID, UpdateUserId = UserID, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });

            //Permission
            permisos.Add(new Permission { Action = "Visualizar", Controller = "Permisos", CreateUserId = UserID, UpdateUserId = UserID, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
            permisos.Add(new Permission { Action = "Crear", Controller = "Permisos", CreateUserId = UserID, UpdateUserId = UserID, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });
            permisos.Add(new Permission { Action = "Editar", Controller = "Permisos", CreateUserId = UserID, UpdateUserId = UserID, CreateDate = DateTime.Now, UpdateDate = DateTime.Now });

            //Ejemplo de Permiso en vista
            //@if (User.HasPermission("Clientes", "Crear")) {}

            foreach (var item in permisos)
                if (!permisosDb.Any(x => x.Action == item.Action && x.Controller == item.Controller))
                    context.Permissions.Add(item);

            context.SaveChanges();

            var permissionRoles = context.PermissionRoles.ToList();

            foreach (var item in context.Permissions)
            {
                if (!permissionRoles.Any(x => x.PermissionId == item.Id && x.RoleId == RoleID))
                    context.PermissionRoles.Add(new PermissionRole
                    {
                        PermissionId = item.Id,
                        RoleId = RoleID
                    });
            }

            context.SaveChanges();

            #endregion
            // TODO Settings por defecto...
        }
    }
}
