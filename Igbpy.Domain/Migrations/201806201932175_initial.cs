namespace Igbpy.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageUrl = c.String(nullable: false),
                        Order = c.Int(nullable: false),
                        Seconds = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Campaigns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        ImageUrl = c.String(nullable: false),
                        WebUrl = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CloudMsgToken",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Token = c.String(),
                        Platform = c.Int(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Imei = c.String(),
                        Platform = c.String(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UsersClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UsersLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UsersRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        UserId = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Generics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageUrl = c.String(),
                        String = c.String(),
                        Location = c.String(),
                        Decimal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Integer = c.Int(nullable: false),
                        Boolean = c.Boolean(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        NDateTime = c.DateTime(),
                        DeleteDate = c.DateTime(),
                        CreateUserId = c.String(nullable: false, maxLength: 128),
                        UpdateUserId = c.String(nullable: false, maxLength: 128),
                        DeleteUserId = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Generic_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreateUserId)
                .ForeignKey("dbo.Users", t => t.DeleteUserId)
                .ForeignKey("dbo.Generics", t => t.Generic_Id)
                .ForeignKey("dbo.Users", t => t.UpdateUserId)
                .Index(t => t.CreateUserId)
                .Index(t => t.UpdateUserId)
                .Index(t => t.DeleteUserId)
                .Index(t => t.Generic_Id);
            
            CreateTable(
                "dbo.LanguageItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                        Value = c.String(),
                        LanguageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Languages", t => t.LanguageId)
                .Index(t => t.LanguageId);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        UserId = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.PermissionRoles",
                c => new
                    {
                        PermissionId = c.Int(nullable: false),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.PermissionId, t.RoleId });
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Controller = c.String(nullable: false),
                        Action = c.String(nullable: false),
                        DeleteDate = c.DateTime(),
                        CreateUserId = c.String(nullable: false, maxLength: 128),
                        UpdateUserId = c.String(nullable: false, maxLength: 128),
                        DeleteUserId = c.String(maxLength: 128),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreateUserId)
                .ForeignKey("dbo.Users", t => t.DeleteUserId)
                .ForeignKey("dbo.Users", t => t.UpdateUserId)
                .Index(t => t.CreateUserId)
                .Index(t => t.UpdateUserId)
                .Index(t => t.DeleteUserId);
            
            CreateTable(
                "dbo.PrintCoordinates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocTypeId = c.Short(nullable: false),
                        DetailBoxLat = c.String(),
                        DetailBoxLong = c.String(),
                        DetailBoxHeight = c.String(),
                        DetailQuantityLimit = c.Int(nullable: false),
                        FontSize = c.String(),
                        LetterSpacing = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QueuedEmails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 200),
                        Body = c.String(nullable: false),
                        IsBodyHtml = c.Boolean(nullable: false),
                        ToAddress = c.String(nullable: false, maxLength: 200),
                        ToName = c.String(maxLength: 200),
                        FromAddress = c.String(nullable: false, maxLength: 200),
                        FromName = c.String(maxLength: 200),
                        CC = c.String(),
                        Bcc = c.String(),
                        ReplyTo = c.String(),
                        AttachmentFilePath = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        LastTryDate = c.DateTime(),
                        PendingRetriesCount = c.Int(nullable: false),
                        LastErrorMessage = c.String(),
                        IsSent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SentCloudMsgs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MessageType = c.Int(nullable: false),
                        Message = c.String(),
                        ExtraData = c.String(),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SentDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 200),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Key);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SentCloudMsgs", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsersRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Permissions", "UpdateUserId", "dbo.Users");
            DropForeignKey("dbo.Permissions", "DeleteUserId", "dbo.Users");
            DropForeignKey("dbo.Permissions", "CreateUserId", "dbo.Users");
            DropForeignKey("dbo.Languages", "UserId", "dbo.Users");
            DropForeignKey("dbo.LanguageItems", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.Generics", "UpdateUserId", "dbo.Users");
            DropForeignKey("dbo.Generics", "Generic_Id", "dbo.Generics");
            DropForeignKey("dbo.Generics", "DeleteUserId", "dbo.Users");
            DropForeignKey("dbo.Generics", "CreateUserId", "dbo.Users");
            DropForeignKey("dbo.Contacts", "UserId", "dbo.Users");
            DropForeignKey("dbo.CloudMsgToken", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsersRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsersLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsersClaims", "UserId", "dbo.Users");
            DropIndex("dbo.SentCloudMsgs", new[] { "UserId" });
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.Permissions", new[] { "DeleteUserId" });
            DropIndex("dbo.Permissions", new[] { "UpdateUserId" });
            DropIndex("dbo.Permissions", new[] { "CreateUserId" });
            DropIndex("dbo.Languages", new[] { "UserId" });
            DropIndex("dbo.LanguageItems", new[] { "LanguageId" });
            DropIndex("dbo.Generics", new[] { "Generic_Id" });
            DropIndex("dbo.Generics", new[] { "DeleteUserId" });
            DropIndex("dbo.Generics", new[] { "UpdateUserId" });
            DropIndex("dbo.Generics", new[] { "CreateUserId" });
            DropIndex("dbo.Contacts", new[] { "UserId" });
            DropIndex("dbo.UsersRoles", new[] { "RoleId" });
            DropIndex("dbo.UsersRoles", new[] { "UserId" });
            DropIndex("dbo.UsersLogins", new[] { "UserId" });
            DropIndex("dbo.UsersClaims", new[] { "UserId" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.CloudMsgToken", new[] { "UserId" });
            DropTable("dbo.Settings");
            DropTable("dbo.SentCloudMsgs");
            DropTable("dbo.Roles");
            DropTable("dbo.QueuedEmails");
            DropTable("dbo.PrintCoordinates");
            DropTable("dbo.Permissions");
            DropTable("dbo.PermissionRoles");
            DropTable("dbo.Languages");
            DropTable("dbo.LanguageItems");
            DropTable("dbo.Generics");
            DropTable("dbo.Contacts");
            DropTable("dbo.UsersRoles");
            DropTable("dbo.UsersLogins");
            DropTable("dbo.UsersClaims");
            DropTable("dbo.Users");
            DropTable("dbo.CloudMsgToken");
            DropTable("dbo.Campaigns");
            DropTable("dbo.Ads");
        }
    }
}
