﻿using Igbpy.Domain.IdentityModels;
using Igbpy.Domain.Shared;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Igbpy.Domain.Models
{
    [Table("Languages")]
    public class Language: BaseModel<int>
    {

        public Language()
        {
            this.LanguageItems = new List<LanguageItem>();
        }

        public string Name { get; set; }

        public string Code { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }

        public virtual List<LanguageItem> LanguageItems { get; set; }
    }
}
