﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    public class PermissionRole
    {

        [Key, Column(Order = 0)]
        [Required]
        public int PermissionId { set; get; }

        [Key, Column(Order = 1)]
        [Required]
        public string RoleId { set; get; }

    }
}
