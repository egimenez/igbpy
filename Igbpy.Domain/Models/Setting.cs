﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Igbpy.Domain.Models
{
    [Table("Settings")]
    public class Setting
    {
        [Key, MaxLength(200)]
        public string Key { get; set; }

        [Required]
        public string Value { get; set; }
    }
}
