﻿using Igbpy.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    [Table("Ads")]
    public class Ads: BaseModel<int>
    {
        [Required]
        public String ImageUrl { get; set; }

        public int Order { get; set; }
        
        public int Seconds { get; set; }
    }
}
