﻿using Igbpy.Domain.IdentityModels;
using Igbpy.Domain.Shared;
using System.ComponentModel.DataAnnotations.Schema;

namespace Igbpy.Domain.Models
{
    [Table("LanguageItems")]
    public class LanguageItem 
    {
       
        public int Id { get; set; }

        public string Key { get; set; }
        public string Value { get; set; }

        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
    }
}
