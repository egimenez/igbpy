﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    [Table("QueuedEmails")]
    public class QueuedEmail
    {
        public int Id { get; set; }

        [Required, MaxLength(200)]
        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }

        public bool IsBodyHtml { get; set; }

        [Required, MaxLength(200)]
        public string ToAddress { get; set; }

        [MaxLength(200)]
        public string ToName { get; set; }

        [Required, MaxLength(200)]
        public string FromAddress { get; set; }

        [MaxLength(200)]
        public string FromName { get; set; }

        public string CC { get; set; }

        public string Bcc { get; set; }

        public string ReplyTo { get; set; }

        public string AttachmentFilePath { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? LastTryDate { get; set; }

        public int PendingRetriesCount { get; set; }
        public string LastErrorMessage { get; set; }

        public bool IsSent { get; set; }
    }
}
