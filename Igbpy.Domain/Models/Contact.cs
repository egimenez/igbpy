﻿using Igbpy.Domain.IdentityModels;
using Igbpy.Domain.Shared;
using System.ComponentModel.DataAnnotations.Schema;

namespace Igbpy.Domain.Models
{
    [Table("Contacts")]
    public class Contact: BaseModel<int>
    {
        public string Message { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
