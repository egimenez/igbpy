﻿using Igbpy.Domain.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{

    [Table("CloudMsgToken")]
    public class CloudMsgToken
    {
        public enum PlatformTypeEnum
        {
            Android = 1,
            iOS = 2
        }

        public int Id { get; set; }

        public string UserId { get; set; }

        public virtual User User { get; set; }

        public string Token { get; set; }

        public int Platform { get; set; }

        public DateTime UpdateDate { get; set; }

        [NotMapped]
        public PlatformTypeEnum PlatformType
        {
            get
            {
                return (PlatformTypeEnum)Platform;
            }
            set
            {
                Platform = (int)value;
            }
        }

    }
}
