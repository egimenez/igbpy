﻿using Igbpy.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    [Table("PrintCoordinates")]
    public class PrintCoordinates : BaseModel<int>
    {

        #region DocType

        public short DocTypeId { get; set; }

        public enum DocType
        {
            ////Invoice
            //[Description("Factura Simple")]
            //SingleInvoice = 1,

            //[Description("Factura Doble")]
            //DoubleInvoice = 2,

            //[Description("Factura Triple")]
            //TripleInvoice = 3,



            ////Budget
            //[Description("Presupuesto Simple")]
            //SingleBudget = 7,

            //[Description("Presupuesto Doble")]
            //DoubleBudget = 8,

            //[Description("Presupuesto Triple")]
            //TripleBudget = 9,



            ////CreditNote
            //[Description("Nota de Crédito Simple")]
            //SingleCreditNote = 4,

            //[Description("Nota de Crédito Doble")]
            //DoubleCreditNote = 5,

            //[Description("Nota de Crédito Triple")]
            //TripleCreditNote = 6,



            ////Receipt
            //[Description("Recibo Simple")]
            //SingleReceipt = 10,

            //[Description("Recibo Doble")]
            //DoubleReceipt = 11,



            ////RemissionNote
            //[Description("Nota de Remisión Simple")]
            //SingleRemisionNote = 12,

            ////Ticket
            //[Description("Ticket Simple")]
            //SingleTicket = 13,



            //Generic
            [Description("Generico Simple")]
            SingleGen = 14,

            [Description("Generico Doble")]
            DoubleGen = 15,

            [Description("Generico Triple")]
            TripleGen = 16,

        }

        #endregion


        #region header

        //public string NumberLat { get; set; }
        //public string NumberLong { get; set; }


        //public string DateStringLat { get; set; }
        //public string DateStringLong { get; set; }


        //public string CheckContadoLat { get; set; }
        //public string CheckContadoLong { get; set; }


        //public string CheckCreditLat { get; set; }
        //public string CheckCreditLong { get; set; }


        //public string NameLat { get; set; }
        //public string NameLong { get; set; }


        //public string LegalIdLat { get; set; }
        //public string LegalIdLong { get; set; }


        //public string AddressLat { get; set; }
        //public string AddressLong { get; set; }


        //public string PhoneLat { get; set; }
        //public string PhoneLong { get; set; }

        #endregion
        

        #region body

        //public string TaxTotalExcemptLat { get; set; }
        //public string TaxTotalExcemptLong { get; set; }


        //public string TaxTotalFiveLat { get; set; }
        //public string TaxTotalFiveLong { get; set; }


        //public string TaxTotalTenLat { get; set; }
        //public string TaxTotalTenLong { get; set; }

        #endregion


        #region detail

        public string DetailBoxLat { get; set; }
        public string DetailBoxLong { get; set; }

        public string DetailBoxHeight { get; set; }
        public int DetailQuantityLimit { get; set; }

        #endregion


        #region footer

        //public string StringTotalLat { get; set; }
        //public string StringTotalLong { get; set; }


        //public string CashStringTotalLat { get; set; }
        //public string CashStringTotalLong { get; set; }


        //public string IntegerTotalLat { get; set; }
        //public string IntegerTotalLong { get; set; }


        //public string SecIntegerTotalLat { get; set; }
        //public string SecIntegerTotalLong { get; set; }


        //public string FootTaxTotalFiveLat { get; set; }
        //public string FootTaxTotalFiveLong { get; set; }


        //public string FootTaxTotalTenLat { get; set; }
        //public string FootTaxTotalTenLong { get; set; }


        //public string FootTaxTotalLat { get; set; }
        //public string FootTaxTotalLong { get; set; }

        #endregion


        #region fontParameters

        public string FontSize { get; set; }
        public string LetterSpacing { get; set; }

        #endregion

    }
}
