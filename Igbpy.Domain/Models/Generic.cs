﻿using Igbpy.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    [Table("Generics")]
    public class Generic: AuditedBaseModel<int>
    {
        public string ImageUrl { get; set; }

        public string String { get; set; }

        public string Location { get; set; }
        
        public decimal Decimal { get; set; }

        public int Integer { get; set; }

        public bool Boolean { get; set; }

        public DateTime DateTime { get; set; }

        public DateTime? NDateTime { get; set; }

        public virtual ICollection<Generic> Details { get; set; }
    }
}
