﻿using Igbpy.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    [Table("Permissions")]
    public class Permission : AuditedBaseModel<int>
    {
            [Required]
            public string Controller { set; get; }

            [Required]
            public string Action { set; get; }
    }
}
