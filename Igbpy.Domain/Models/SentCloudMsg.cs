﻿using Igbpy.Domain.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Models
{
    [Table("SentCloudMsgs")]
    public class SentCloudMsg
    {
        public int Id { get; set; }
        public int MessageType { get; set; }
        public string Message { get; set; }
        public string ExtraData { get; set; }

        [Required]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public DateTime SentDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool Deleted { get; set; }
    }
}
