﻿using Igbpy.Domain.IdentityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Shared
{
    /// <summary>
    /// Modelo básico para entidades con campo de identificador único (tipo definible) y
    /// campos de fecha de creación, actualización e indicador de elemento eliminado.
    /// </summary>
    /// <typeparam name="T">Tipo de dato de la clave principal de la entidad</typeparam>
    public abstract class BaseModel<T>
    {
        public T Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool Deleted { get; set; }
    }

    /// <summary>
    /// Modelo básico con campos de auditoría extendidos. Fecha de eliminación e identificadores
    /// de los usuarios de creación, última actualización y eliminación.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AuditedBaseModel<T>: BaseModel<T>
    {
        public DateTime? DeleteDate { get; set; }

        [Required]
        public string CreateUserId { get; set; }
        public virtual User CreateUser { get; set; }

        [Required]
        public string UpdateUserId { get; set; }
        public virtual User UpdateUser { get; set; }

        public string DeleteUserId { get; set; }
        public virtual User DeleteUser { get; set; }
    }
}
