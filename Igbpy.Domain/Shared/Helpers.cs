﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Shared
{
    public static class Helpers
    {

        #region Send Emails


        /// <summary>
        /// Envía un listado de emails. Se tratará de realizar el envío al menos una vez para cada receptor, siendo invocada una función de callback
        /// que debe incluirse como parámetro para controlar que hacer en caso de envío correcto o error.
        /// </summary>
        /// <param name="mailMessages">Listado de mensajes a enviar</param>
        /// <param name="host">Host del servidor de email</param>
        /// <param name="port">Puerto del servidor de email</param>
        /// <param name="enableSsl">Indicador para utilizar SSL en el envío</param>
        /// <param name="credentials">Creadenciales a utilizar para acceder al servidor de SMTP</param>
        /// <param param name="onSend">Funcion de callback que será llamada cuando finalice el envío de un mensaje (una vez por mensaje)</param>
        /// <param name="onFailure">Función de callback que será llamada cuando haya un error que provoque una excepcion en el envío de un mensaje.</param>
        public static void SendEmail(IEnumerable<MailMessage> mailMessages, string host, int port, bool enableSsl, ICredentialsByHost credentials = null, Action<MailMessage> onSend = null, Action<MailMessage, Exception> onFailure = null)
        {
            // SE prepara el cliente de envío de emails
            SmtpClient smtpClient = new SmtpClient(host, port);
            smtpClient.EnableSsl = enableSsl;
            smtpClient.UseDefaultCredentials = credentials == null;
            if (credentials != null)
                smtpClient.Credentials = credentials;

            // Se envía cada mensaje y se invocan los callbacks apropiados dependiendo del resultado
            foreach (var mailMessage in mailMessages)
            {
                try
                {
                    smtpClient.Send(mailMessage);
                    onSend?.Invoke(mailMessage);
                }
                catch (Exception e)
                {
                    onFailure?.Invoke(mailMessage, e);
                }
            }
        }

        /// <summary>
        /// Envía un listado de emails. Se tratará de realizar el envío al menos una vez para cada receptor, siendo invocada una función de callback
        /// que debe incluirse como parámetro para controlar que hacer en caso de envío correcto o error.
        /// </summary>
        /// <param name="from">Datos del emisor</param>
        /// <param name="to">Datos del receptor</param>
        /// <param name="subject">Asunto del email</param>
        /// <param name="body">Cuerpo del email</param>
        /// <param name="host">Host del servidor de email</param>
        /// <param name="port">Puerto del servidor de email</param>
        /// <param name="enableSsl">Indicador para utilizar SSL en el envío</param>
        /// <param name="credentials">Creadenciales a utilizar para acceder al servidor de SMTP</param>
        /// <param name="isBodyHtml">Indicador para que se trate el cuerpo del mensaje como HTML (true por defecto)</param>
        /// <param name="cc">Listado de emails separados por comas a los que se enviará una copia del email</param>
        /// <param name="bcc">Listado de emails separados por comas a los que se enviará una copia oculta del email</param>
        /// <param name="replyTo">Listado de emails sepados por comas que serán los receptores por defecto cuando se ejecute la operación "reply to" en el receptor</param>
        /// <param name="attachmentFileNames">Listado de rutas a ficheros que adjuntar al email</param>
        /// <param param name="onSend">Funcion de callback que será llamada cuando finalice el envío de un mensaje (una vez por mensaje)</param>
        /// <param name="onFailure">Función de callback que será llamada cuando haya un error que provoque una excepcion en el envío de un mensaje.</param>
        public static void SendEmail(MailAddress from, IEnumerable<MailAddress> to, string subject, string body, string host, int port, bool enableSsl, ICredentialsByHost credentials = null, bool isBodyHtml = true, string cc = null, string bcc = null, string replyTo = null, string[] attachmentFileNames = null, Action<MailMessage> onSend = null, Action<MailMessage, Exception> onFailure = null)
        {
            // Se prepara un mensaje para cada receptor
            var mailMessages = to.Select(toAddress =>
            {
                MailMessage mailMessage = new MailMessage(from, toAddress)
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                };
                try
                {
                    if (!string.IsNullOrEmpty(cc))
                        mailMessage.CC.Add(cc);
                    if (!string.IsNullOrEmpty(bcc))
                        mailMessage.Bcc.Add(bcc);
                    if (!string.IsNullOrEmpty(replyTo))
                        mailMessage.ReplyToList.Add(replyTo);
                    if (attachmentFileNames != null)
                    {
                        foreach (string fileName in attachmentFileNames)
                        {
                            mailMessage.Attachments.Add(new Attachment(fileName));
                        }
                    }
                }
                catch (Exception e)
                {
                    // Si hay un error al preparar el mensaje, se informa y no se enviará
                    onFailure?.Invoke(mailMessage, e);
                    mailMessage = null;
                }
                return mailMessage;
            }).Where(x => x != null).ToArray();

            // Se envían los mensajes
            SendEmail(
                mailMessages: mailMessages,
                host: host,
                port: port,
                enableSsl: enableSsl,
                credentials: credentials,
                onSend: onSend,
                onFailure: onFailure);
        }

        /// <summary>
        /// Pone una lista de emails en cola para ser enviadas.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isBodyHtml"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>
        /// <param name="replyTo"></param>
        public static void QueueEmail(MailAddress from, IEnumerable<MailAddress> to, string subject, string body, bool isBodyHtml = true, string cc = null, string bcc = null, string replyTo = null)
        {
            // Se accede a la base de datos
            using (IgbpyContext context = new IgbpyContext())
            {
                // Se crea una entidad para cada mensaje a enviar
                foreach (var toAddress in to)
                {
                    context.QueuedEmails.Add(new Domain.Models.QueuedEmail
                    {
                        FromAddress = from.Address,
                        FromName = from.DisplayName,
                        ToAddress = toAddress.Address,
                        ToName = toAddress.DisplayName,
                        Bcc = bcc,
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = isBodyHtml,
                        CreateDate = DateTime.Now,
                    });
                }
                // Se guardan los cambios
                context.SaveChanges();
            }
        }

        #endregion

    }
}
