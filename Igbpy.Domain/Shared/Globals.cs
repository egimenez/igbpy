﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Shared
{
    public static class Globals
    {
        public static class Roles
        {
            public const string Admin = "Administrador";

            public static readonly string[] AllRoles = { Admin };
        }

        public static class Platforms
        {
            public const string Web = "WEB";
            public const string Mobile = "MOBILE";
        }

        public static class DatabaseDefaults
        {
            public static class AdminUser
            {
                public const string UserName = "admin@admin.com";
                public const string Email = "admin@admin.com";
                public const string Password = "123456";
            }

            public static class GuestUser
            {
                public const string UserName = "guestuser_Igbpy@noemail.com";
                public const string Email = "guestuser_Igbpy@noemail.com";
            }

            public static class QueuedEmails
            {
                public const int PendingRetries = 3;
            }

            public static class UserValidation
            {
                public const int PasswordMinLength = 6;
            }
        }

        public static class CommonSettings
        {
            //public const string UseWindowsService = "CommonSettings_UseWindowsService";
        }

        public static class Regex
        {
            public const string Url = @"^(http|https):\/\/.+";
            public const string UrlOrEmpty = @"(^$|"+Url+")";
            public const string Email = @"^.+@.+$";
            public const string EmailOrEmpty = @"(^$|" + Email + ")";
        }
    }
}
