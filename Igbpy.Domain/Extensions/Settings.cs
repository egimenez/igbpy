﻿using Igbpy.Domain.Models;
using Igbpy.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Extensions.Settings
{
    public static class Extensions
    {

        private static Func<PropertyInfo, string> GetSettingKeyDelegate(Type typeOfSetting)
        {
            string name = typeOfSetting.Name;
            return new Func<PropertyInfo, string>(x => name + "_" + x.Name);
        }

        private static IEnumerable<KeyValuePair<PropertyInfo, Setting>> GetPropertySettings(IgbpyContext db, Type typeOfSettings)
        {
            var name = typeOfSettings.Name;
            var properties = typeOfSettings.GetProperties().Where(x => x.CanRead && x.CanWrite);

            var settingsKeys = properties.Select(GetSettingKeyDelegate(typeOfSettings)).ToArray();
            var currentSettings = db.Settings.Where(x => settingsKeys.Contains(x.Key)).ToList();
            var settingsKeyDelegate = GetSettingKeyDelegate(typeOfSettings);

            var propertiesSettings = from p in properties from s in currentSettings.Where(x => x.Key == settingsKeyDelegate(p)).DefaultIfEmpty() select new KeyValuePair<PropertyInfo, Setting>(p, s);
            return propertiesSettings;
        }

        private static Task<IEnumerable<KeyValuePair<PropertyInfo, Setting>>> GetPropertySettingsAsync(IgbpyContext db, Type typeOfSettings)
        {
            var name = typeOfSettings.Name;
            var properties = typeOfSettings.GetProperties().Where(x => x.CanRead && x.CanWrite);

            var settingsKeys = properties.Select(GetSettingKeyDelegate(typeOfSettings)).ToArray();
            var currentSettings = db.Settings.Where(x => settingsKeys.Contains(x.Key)).ToList();
            var settingsKeyDelegate = GetSettingKeyDelegate(typeOfSettings);

            var propertiesSettings = from p in properties from s in currentSettings.Where(x => x.Key == settingsKeyDelegate(p)).DefaultIfEmpty() select new KeyValuePair<PropertyInfo, Setting>(p, s);
            return Task.Factory.StartNew<IEnumerable<KeyValuePair<PropertyInfo, Setting>>>(() =>
            {
                return GetPropertySettings(db, typeOfSettings);
            });
        }

        private static void UpdateSettings<T>(this IgbpyContext db, T settings)
        {
            var typeOfSettigns = typeof(T);
            var settingsKeyDelegate = GetSettingKeyDelegate(typeOfSettigns);

            foreach (var propertySetting in GetPropertySettings(db, typeOfSettigns))
            {
                PropertyInfo property = propertySetting.Key;
                Setting setting = propertySetting.Value;
                if (setting == null)
                {
                    setting = new Setting { Key = settingsKeyDelegate(property) };
                    db.Settings.Add(setting);
                }

                setting.Value = Convert.ToString(property.GetValue(settings));

                if (string.IsNullOrEmpty(setting.Value))
                    db.Settings.Remove(setting);
            }
        }

        private static Task UpdateSettingsAsync<T>(this IgbpyContext db, T settings)
        {
            return Task.Factory.StartNew(() =>
            {
                db.UpdateSettings<T>(settings);
            });
        }

        private static T GetSettings<T>(this IgbpyContext db)
        {
            var typeOfSettigns = typeof(T);
            object settings = typeOfSettigns.GetConstructor(new Type[] { }).Invoke(null);

            foreach (var propertySetting in GetPropertySettings(db, typeOfSettigns))
            {
                PropertyInfo property = propertySetting.Key;
                Setting setting = propertySetting.Value;
                if (setting != null)
                    property.SetValue(settings, Convert.ChangeType(setting.Value, property.PropertyType));
            }

            return (T)settings;
        }

        private static Task<T> GetSettingsAsync<T>(this IgbpyContext db)
        {
            return Task.Factory.StartNew<T>(() =>
            {
                return db.GetSettings<T>();
            });

        }

        /// <summary>
        /// Obtiene la información de configuración para el envío de emails
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Task<EmailSettings> GetEmailSettingsAsync(this IgbpyContext db)
        {
            // Se retorna
            return db.GetSettingsAsync<EmailSettings>();
        }

        /// <summary>
        /// Guarda la información de configuración para el envío de emails, esta operacion
        /// no invoca la operacion Db.SaveChanges, debe hacerse de forma externa para que los
        /// cambios se mantengan.
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Task UpdateEmailSettingsAsync(this IgbpyContext db, EmailSettings modelo)
        {
            return db.UpdateSettingsAsync(modelo);
        }

        /// <summary>
        /// Obtiene la información de configuración para el uso de productos de Google
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Task<GoogleSettings> GetGoogleSettingsAsync(this IgbpyContext db)
        {
            // Se retorna
            return db.GetSettingsAsync<GoogleSettings>();
        }

        /// <summary>
        /// Guarda la información de configuración para los productos de Google, esta operacion
        /// no invoca la operacion Db.SaveChanges, debe hacerse de forma externa para que los
        /// cambios se mantengan.
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Task UpdateGoogleSettingsAsync(this IgbpyContext db, GoogleSettings modelo)
        {
            return db.UpdateSettingsAsync(modelo);
        }

        /// <summary>
        /// Obtiene la información de configuración propia para productos de Apple (notificaciones, etc)
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Task<AppleSettings> GetAppleSettingsAsync(this IgbpyContext db)
        {
            // Se retorna
            return db.GetSettingsAsync<AppleSettings>();
        }

        /// <summary>
        /// Guarda la información de configuración para las configuraciones propias de Apple, esta operacion
        /// no invoca la operacion Db.SaveChanges, debe hacerse de forma externa para que los
        /// cambios se mantengan.
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Task UpdateAppleSettingsAsync(this IgbpyContext db, AppleSettings modelo)
        {
            return db.UpdateSettingsAsync(modelo);
        }
    }

    public class EmailSettings
    {
        public string Host { get; set; }
        public string Pass { get; set; }
        public int Port { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string User { get; set; }
        public bool EnableSsl { get; set; }
        public string WebSiteDefaultBccAddress { get; set; }
        public string WebSiteDefaultSenderAddress { get; set; }

    }

    public class GoogleSettings
    {
        public string BrowserApiKey { get; set; }
        public string ServerGcmKey { get; set; }
    }

    /// <summary>
    /// Settings de Apple
    /// </summary>
    public class AppleSettings
    {
        public enum PushEnvironments
        {
            Sandbox = 1,
            Production = 2
        }
        public int PushEnvironment { get; set; }

        /// <summary>
        /// Objeto de tipo CertificateFile serializado como JSON. O null.
        /// </summary>
        public string PushCertificateFile { get; set; }
        public string PushCertificatePassword { get; set; }

        public PushEnvironments GetPushEnvironment()
        {
            if (Enum.IsDefined(typeof(PushEnvironments), PushEnvironment))
                return (PushEnvironments)PushEnvironment;
            return PushEnvironments.Sandbox;
        }

        public void SetPushEnvironment(PushEnvironments environment)
        {
            PushEnvironment = (int)environment;
        }
    }
}
