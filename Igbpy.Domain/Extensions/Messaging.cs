﻿using Igbpy.Domain.Extensions.Settings;
using Igbpy.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Igbpy.Domain.Extensions.Messaging
{
    /// <summary>
    /// Métodos de extension para el contexto relacionados con el envío de mensajes (email, push, sms) utilizando
    /// los settings propios de este modelo
    /// </summary>
    public static class Extensions
    {
        #region Send Emails


        /// <summary>
        /// Envía un listado de emails con los settings de email establecidos. 
        /// Se tratará de realizar el envío al menos una vez para cada receptor, siendo invocada una función de callback
        /// que debe incluirse como parámetro para controlar que hacer en caso de envío correcto o error.
        /// </summary>
        /// <param name="mailMessages">Listado de mensajes a enviar</param>
        /// <param param name="onSend">Funcion de callback que será llamada cuando finalice el envío de un mensaje (una vez por mensaje)</param>
        /// <param name="onFailure">Función de callback que será llamada cuando haya un error que provoque una excepcion en el envío de un mensaje.</param>
        public static void SendEmails(this IgbpyContext db, IEnumerable<MailMessage> mailMessages, Action<MailMessage> onSend = null, Action<MailMessage, Exception> onFailure = null, bool includeDefaultBcc = true)
        {
            // Se obtienen los settings para el envío de mensajes
            EmailSettings settings = db.GetEmailSettingsAsync().GetAwaiter().GetResult();

            // Si hay algún problema con los datos, se trata de un error
            if (string.IsNullOrEmpty(settings.Host) || string.IsNullOrEmpty(settings.WebSiteDefaultSenderAddress) || (!settings.UseDefaultCredentials && (string.IsNullOrEmpty(settings.User) || string.IsNullOrEmpty(settings.Pass))))
                throw new Exception("La configuracion de email no es correcta");

            // se revisan los mensajes para incluir el emisor por defecto en caso de que no tenga, y los destinatarios
            // de copia oculta de ser necesario
            mailMessages = mailMessages.Select(x =>
            {
                if (x.From == null)
                    x.From = new MailAddress(settings.WebSiteDefaultSenderAddress);
                if (includeDefaultBcc && !string.IsNullOrEmpty(settings.WebSiteDefaultBccAddress))
                    x.Bcc.Add(settings.WebSiteDefaultBccAddress);
                return x;
            });


            // SE prepara el cliente de envío de emails
            Helpers.SendEmail(mailMessages, settings.Host, settings.Port, settings.EnableSsl, !settings.UseDefaultCredentials ? new NetworkCredential(settings.User, settings.Pass) : null, onSend, onFailure);
        }

        /// <summary>
        /// Pone una lista de emails en cola para ser enviadas usando los settings de Email establecidos.
        /// IMPORTANTE: No se guardarán los cambios en la base de datos hasta que se utilice "SaveChanges" externamente.
        /// </summary>
        /// <param name="mailMessages">Emails a enviar. En caso de que "from" no tenga valor, se asignará el valor por defecto.
        /// Se ignorarán los ficheros adjuntos que pudieran estar asignados a los mails pasados como parametro.</param>
        /// <param name="includeDefaultBcc">Indicador para incluir los receptores de copia oculta por defecto a los emails a enviar.</param>
        /// <param name="attachmentFilePath">Ruta de servidor del fichero adjunto a incluir en los emails a enviar.</param>
        public static void QueueEmails(this IgbpyContext db, IEnumerable<MailMessage> mailMessages, bool includeDefaultBcc = true, string attachmentFilePath = null)
        {
            // Se obtienen los settings para el envío de mensajes
            EmailSettings settings = db.GetEmailSettingsAsync().GetAwaiter().GetResult();

            foreach (var mailMessage in mailMessages)
            {
                // Se crea una entidad para cada mensaje a enviar

                // Se preparan los datos a almacenar
                IEnumerable<string> bccAddresses = new string[0];
                if (includeDefaultBcc && !string.IsNullOrEmpty(settings.WebSiteDefaultBccAddress))
                    bccAddresses = settings.WebSiteDefaultBccAddress.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                string bcc = string.Join(",", bccAddresses.Concat(mailMessage.Bcc.Select(x => x.Address))),
                    cc = string.Join(",", mailMessage.CC.Select(x => x.Address)),
                    replyTo = string.Join(",", mailMessage.ReplyToList.Select(x => x.Address));

                db.QueuedEmails.Add(new Domain.Models.QueuedEmail
                {
                    FromAddress = mailMessage.From != null ? mailMessage.From.Address : settings.WebSiteDefaultSenderAddress,
                    FromName = mailMessage.From?.DisplayName,
                    ToAddress = mailMessage.To.First().Address,
                    ToName = mailMessage.To.First().DisplayName,
                    CC = !string.IsNullOrEmpty(cc) ? cc : null,
                    Bcc = !string.IsNullOrEmpty(bcc) ? bcc : null,
                    ReplyTo = !string.IsNullOrEmpty(replyTo) ? replyTo : null,
                    Subject = mailMessage.Subject,
                    AttachmentFilePath = attachmentFilePath,
                    Body = mailMessage.Body,
                    IsBodyHtml = mailMessage.IsBodyHtml,
                    CreateDate = DateTime.Now,
                    PendingRetriesCount = Globals.DatabaseDefaults.QueuedEmails.PendingRetries
                });
            }
        }

        #endregion
    }
}
