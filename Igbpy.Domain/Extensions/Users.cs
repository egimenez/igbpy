﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;

namespace Igbpy.Domain.Extensions.Users
{
    public static class Extensions
    {
        public static bool HasPermission(this IdentityUser User, string controller, string action) {


            return HasPermission(User.Id, controller, action);


        }

        public static bool HasPermission(this System.Security.Principal.IPrincipal User, string controller, string action)
        {


            return HasPermission(User.Identity.GetUserId(), controller, action);


        }

        public static bool HasPermission(string userId, string controller, string action) {

            IgbpyContext db = new IgbpyContext();

            var userRoles = db.Roles.Where(x => x.Users.Any(y => y.UserId == userId)).Select(x => x.Id);
            var rolesPermissions = db.PermissionRoles.Where(x => userRoles.Contains(x.RoleId)).Select(x => x.PermissionId);
            var permissions = db.Permissions.Where(x => rolesPermissions.Contains(x.Id));

            return permissions.FirstOrDefault(x => x.Controller.ToLower() == controller.ToLower() && x.Action.ToLower() == action.ToLower()) != null;
        }

    }
}
